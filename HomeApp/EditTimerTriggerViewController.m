//
//  EditTimerTriggerViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditTimerTriggerViewController.h"
#import "TableViewNameCell.h"
#import "TableViewDatePickerCell.h"
#import "TableViewSwitchCell.h"
#import "HomeKitManager.h"
#import <HomeKit/HomeKit.h>


@interface EditTimerTriggerViewController () {
    UIDatePicker *_datePicker;
    
    HMTimerTrigger *timerTrigger;
    NSMutableArray *recurrenceTitles;
    
    NSDate *fireDate;
    NSDateComponents *recurrenceComponentsForTrigger;
    
    int selectedRecurrence;
}
@end


@implementation EditTimerTriggerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DatePickerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DatePickerCell"];

    timerTrigger = (HMTimerTrigger*)self.trigger;
    recurrenceTitles = [[NSMutableArray alloc] initWithObjects:@"Every Hour", @"Every Day", @"Every Week", nil];
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];

//    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    [super loadData];
    
    selectedRecurrence = self.isAdding ? -1 : [self recurrenceIndexFromDateComponents:timerTrigger.recurrence];

    [self.tableView reloadData];
}

-(int)recurrenceIndexFromDateComponents:(NSDateComponents*)components {

    if (components.day == 1) {
        return 1;
    }
    else if (components.weekOfYear == 1) {
        return 2;
    }
    else if (components.hour == 1) {
        return 0;
    }
    return -1;
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    
    NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    if(self.isAdding) {
        self.saveButton.enabled = (trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName]);
        
    } else {
        self.saveButton.enabled = true;
    }
}

-(void)saveButtonPressed {
    
    if(self.isAdding) {
        
        NSString *triggerName = [self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        
        HMTimerTrigger *newTimerTrigger = [[HMTimerTrigger alloc] initWithName:triggerName fireDate:fireDate timeZone:[NSCalendar currentCalendar].timeZone recurrence:recurrenceComponentsForTrigger recurrenceCalendar:nil];
        
        
        [[HomeKitManager sharedManager] addTrigger:newTimerTrigger toHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error) {
            
            if(error) {
                NSLog(@"Error adding timer trigger %@", error);
                
            } else {
                
                [self addAllSelectedActionSetsToTrigger:newTimerTrigger];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self enableTrigger:newTimerTrigger];
                });
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    } else {
        
        NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        if(trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName])
           [self updateTriggerName:trimmedName];

        
        if(![timerTrigger.fireDate isEqualToDate:fireDate])
            [self updateTriggerFireDate:timerTrigger];
        
        
        int oldRecurrence = [self recurrenceIndexFromDateComponents:timerTrigger.recurrence];
        if(selectedRecurrence != oldRecurrence)
            [self updateRecurrence:timerTrigger];
        

        if((self.selectedActionSets != nil && self.selectedActionSets.count > 0)
           && self.selectedActionSets.count != self.trigger.actionSets.count) {
            
            [self removeAllActionSetsFromTrigger:self.trigger];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addAllSelectedActionSetsToTrigger:self.trigger];
            });
        }
        
        
        if(self.trigger.isEnabled != self.enabledSwitch.on)
            [self enableTrigger:self.trigger];
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - Timer Trigger update methods

-(void)updateTriggerFireDate:(HMTimerTrigger*)trigger {
    [trigger updateFireDate:fireDate completionHandler:^(NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Error updating fireDate on timer trigger %@", error);
            
        } else {
            NSLog(@"Timer Trigger's fireDate updated");
        }
    }];
}

-(void)updateRecurrence:(HMTimerTrigger*)trigger {
    [trigger updateRecurrence:recurrenceComponentsForTrigger completionHandler:^(NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Error updating recurrence on timer trigger %@", error);
            
        } else {
            NSLog(@"Timer Trigger's recurrence updated");
        }
    }];
}


#pragma mark - TableViewDatePickerCell Delegate

-(void) datePickerDidChanged {
    NSDate *rawFireDate = _datePicker.date;
    
    NSDateComponents *comps = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:rawFireDate];
    NSDate *probableDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    fireDate = probableDate == nil ? rawFireDate : probableDate;
    [self enableSaveButtonIfNecessary];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {

        case DateAndTime:
            return @"Date & Time";
            
        case Recurrence:
            return @"Recurrence";
            
        default:
            return [super tableView:tableView titleForHeaderInSection:section];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case Recurrence:
            return recurrenceTitles.count;
            
        default:
            return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 2) {
        return 163.0f;
    }
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case DateAndTime:
            return [self datePickerCellForRowAtIndexPath:indexPath];
            
        case Recurrence:
            return [self recurrenceCellForRowAtIndexPath:indexPath];
            
        case Unknown:
            return nil;
            
        default:
            return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Recurrence:
            [self didSelectRecurrenceComponentAtIndexPath:indexPath];
            break;
            
        default:
            [super tableView:tableView didSelectRowAtIndexPath:indexPath];
            break;
    }
}


#pragma mark - TableView datasource Helper

-(TriggerTableViewSection)sectionForIndex:(NSInteger)section {
    switch (section) {
        case 0:
            return Name;
            
        case 1:
            return Enabled;
            
        case 2:
            return DateAndTime;
            
        case 3:
            return Recurrence;
            
        case 4:
            return ActionSets;
            
        default:
            return Unknown;
    }
}

-(TableViewDatePickerCell*)datePickerCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    TableViewDatePickerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DatePickerCell"];
    if (cell == nil) {
        cell = [[TableViewDatePickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DatePickerCell"];
    }
    _datePicker = cell.datePicker;
    cell._delegate = self;
    
    cell.datePicker.date = self.isAdding ? [[NSDate alloc] init] : timerTrigger.fireDate;
    fireDate = cell.datePicker.date;
    return cell;
}

-(UITableViewCell*)recurrenceCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"RecurrenceCell"];
    cell.textLabel.text = [recurrenceTitles objectAtIndex:indexPath.row];
    
    cell.accessoryType = indexPath.row == selectedRecurrence ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}

-(void)didSelectRecurrenceComponentAtIndexPath:(NSIndexPath*)indexPath {
    
    recurrenceComponentsForTrigger = [[NSDateComponents alloc] init];
    switch (indexPath.row) {
        case 0:
            recurrenceComponentsForTrigger.hour = 1;
            break;
            
        case 1:
            recurrenceComponentsForTrigger.day = 1;
            break;
            
        case 2:
            recurrenceComponentsForTrigger.weekOfYear = 1;
            break;
            
        default:
            break;
    }
    
    selectedRecurrence = (int)indexPath.row;
    [self enableSaveButtonIfNecessary];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
