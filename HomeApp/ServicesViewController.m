//
//  ServicesViewController.m
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import "AppDelegate.h"
#import "ServicesViewController.h"
#import "CharacteristicsViewController.h"

@interface ServicesViewController (){
    NSMutableArray *accessoryServices;
}

@end

@implementation ServicesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.selectedAccessory.name;

    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.selectedAccessory) {
        [self loadData];
    }
}


#pragma mark - Loading Data

-(void) loadData {
    self.selectedAccessory.delegate = self;
    accessoryServices = [[NSMutableArray alloc] initWithArray:self.selectedAccessory.services];
    [self.tableView reloadData];
}


#pragma mark - Button Actions

- (IBAction)renameAccessory:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Rename Accessory" message:@"Type new name for this accessory" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:nil];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
        
        [self.selectedAccessory updateName:textField.text completionHandler:^(NSError *error){
            
            self.title = self.selectedAccessory.name;
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateServices:(HMAccessory *)accessory {
    [self loadData];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Services";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return accessoryServices.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ServiceCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [[accessoryServices objectAtIndex:indexPath.row] name];
    //Released Version return UUID
    //cell.detailTextLabel.text = [[accessoryServices objectAtIndex:indexPath.row] serviceType];
    AppDelegate *myDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *strServiceType = [myDelegate.HomeKitUUIDs objectForKey:[[accessoryServices objectAtIndex:indexPath.row] serviceType]];
    cell.detailTextLabel.text = strServiceType;

    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    
    CharacteristicsViewController *serviceDetailsVC = (CharacteristicsViewController*)[segue destinationViewController];
    serviceDetailsVC.selectedService = [accessoryServices objectAtIndex:indexPath.row];
}

@end
