//
//  SettingsViewController.m
//  Favorite Contacts
//
//  Created by Alex Keller on 28.08.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import "SettingsViewController.h"
#import <KINWebBrowser/KINWebBrowserViewController.h>
#import "ImpressumViewController.h"




@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.formController = [[FXFormController alloc] init];
    self.formController.tableView = self.tableView;
    self.formController.delegate = self;
    self.formController.form = [[SettingsForm alloc] init];
    self.formController.tableView.backgroundColor = [UIColor clearColor];
    self.title = @"Einstellungen";
    
   
    sf = (SettingsForm *)self.formController.form;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //reload the table
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Settings delegates

- (void)open2peaches {
    
    KINWebBrowserViewController *webViewController = [[KINWebBrowserViewController alloc] init];
    webViewController.barTintColor = UIColorFromRGB(0x0958cf);
    webViewController.parentViewController.view.backgroundColor = [UIColor whiteColor];
    webViewController.tintColor = [UIColor whiteColor];
    webViewController.showsURLInNavigationBar = NO;
    [webViewController loadURL:[NSURL URLWithString:@"http://www.2peaches.de"]];
    
    [self.navigationController pushViewController:webViewController animated:YES];
}


- (void)useKeyboard {

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (sf.useKeyboard) {
        [userDefaults setObject:@"AN" forKey:@"useKeyboard"];
        
    }else{
        [userDefaults setObject:@"AUS" forKey:@"useKeyboard"];

        
    }
    [userDefaults synchronize];
}


- (void)rateApp {
    
    NSString* url = [NSString stringWithFormat: @"itms-apps://itunes.apple.com/app/id%@", @"333210224"];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}

- (void)facebook {
    
    NSURL *fanPageURL = [NSURL URLWithString:@"fb://profile/460326684016739"];
    if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
        
        KINWebBrowserViewController *webViewController = [[KINWebBrowserViewController alloc] init];
        webViewController.barTintColor = UIColorFromRGB(0x0958cf);
        webViewController.parentViewController.view.backgroundColor = [UIColor whiteColor];
        webViewController.tintColor = [UIColor whiteColor];
        webViewController.showsURLInNavigationBar = NO;
        [webViewController loadURL:[NSURL URLWithString:@"https://www.facebook.com/2peaches"]];
        
        [self.navigationController pushViewController:webViewController animated:YES];
    
    }
    
}

- (void)feedback {
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    if(controller != nil){
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Kennzeichen-Finder Feedback"];
        [controller setToRecipients:[NSArray arrayWithObject:@"kennzeichenfinder@2peaches.de"]];
        [self presentViewController:controller animated:YES completion:nil];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Kein E-Mail-Account" message:@"Leider können Sie uns keine Anfrage schicken, da Sie keinen Mailaccount in Ihrem iPhone hinterlegt haben. Senden Sie uns doch eine E-Mail an:\nbussgeldrechner@2peaches.de" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (NSDictionary *)countrysField
{
    return @{FXFormFieldOptions: @[@(Deutschland), @(Schweiz), @(Oesterreich)],
             FXFormFieldValueTransformer: ^(id input) {
                 return @{@(Deutschland): @"Deutschland",
                          @(Schweiz): @"Schweiz",
                          @(Oesterreich): @"Österreich"}[input];
             }};
}

- (void) impressum {
    
    ImpressumViewController *impressumViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"impressum"];
    [self.navigationController pushViewController:impressumViewController animated:YES];
    
    
}

#pragma mark - Image background
- (UIImage *)imageWithView:(UIView *)view
{
    
    CGRect screenRect = CGRectMake(0, 0, 0, 0);
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        screenRect = CGRectMake(0, 0, 768, 1024);
    else
        screenRect = [[UIScreen mainScreen] bounds];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(screenRect.size.width, screenRect.size.height+72), self.view.window.opaque, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [self.view.window.layer renderInContext:ctx];
    
    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image1;
}

- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Mailcomposer Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self becomeFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end