//
//  SettingsFontViewController.m
//  
//
//  Created by Tim on 01.09.14.
//
//

#import "SettingsFontViewController.h"

@interface SettingsFontViewController ()

@end

@implementation SettingsFontViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *_oView = [[UIView alloc] initWithFrame:CGRectMake(0, -self.navigationController.navigationBar.frame.size.height-20, self.view.bounds.size.width, self.view.bounds.size.height+self.navigationController.navigationBar.frame.size.height+20)];
    
    CGRect fr = self.view.frame;
    fr.origin.y -= 20;
    fr.size.height += 20;
    self.view.frame = fr;
    
    [_oView addSubview:self.view];
    _oView.backgroundColor = [UIColor greenColor];
    self.view = _oView;
    
    // Do any additional setup after loading the view.

    self.view.layer.contents = (id)[UIImage imageNamed:@"background.png"].CGImage;
    self.formController.tableView.backgroundColor = [UIColor clearColor];

    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.navigationController.navigationBar.translucent = YES;
    
    
    [self.formController enumerateFieldsWithBlock:^(FXFormField *field, NSIndexPath *indexPath) {
        
        [field setValue:[UIFont fontWithName:field.title size:17] forKey:@"textLabel.font"];
    }];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
