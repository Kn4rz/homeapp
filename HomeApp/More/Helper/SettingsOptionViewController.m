//
//  SettingsOptionViewController.m
//  Favorite Contacts
//
//  Created by Tim on 01.09.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import "SettingsOptionViewController.h"

@interface SettingsOptionViewController ()

@end

@implementation SettingsOptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -self.navigationController.navigationBar.frame.size.height-20, self.view.bounds.size.width, self.view.bounds.size.height+self.navigationController.navigationBar.frame.size.height+20)];
    imgView.image = [UIImage imageNamed:@"background.png"];
    [self.view insertSubview:imgView atIndex:0];
    self.view.backgroundColor = [UIColor clearColor];
    self.formController.tableView.scrollEnabled = NO;

    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.navigationController.navigationBar.translucent = YES;


    [self.formController enumerateFieldsWithBlock:^(FXFormField *field, NSIndexPath *indexPath) {
       [field setValue:[UIFont fontWithName:@"HelveticaNeue-Light" size:17] forKey:@"textLabel.font"];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
