//
//  SettingsForm.m
//  Favorite Contacts
//
//  Created by Tim on 01.09.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import "SettingsForm.h"
#import "SettingsOptionViewController.h"
#import "SettingsFontViewController.h"
@implementation SettingsForm
@synthesize sound;

- (NSArray *)fields
{

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
        sound = YES;
    }else {
        sound = NO;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"KennzeichenUpdatedAt"]];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:UIColorFromRGB(0x0863d5)];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithDescriptor:[UIFontDescriptor fontDescriptorWithFontAttributes:@{@"NSCTFontUIUsageAttribute" : UIFontTextStyleBody,
                                                                                                                                                                          @"NSFontNameAttribute" : @"HelveticaNeue-Thin"}] size:12.0]];

    

    return @[
 
             @{FXFormFieldHeader:@"Konfiguration", FXFormFieldKey: @"useKeyboard", FXFormFieldTitle: @"Tastatur benutzen", FXFormFieldAction: @"useKeyboard", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17]},
            
             @{FXFormFieldKey: @"countrys", FXFormFieldTitle:@"Land wählen", FXFormFieldOptions: @[@"Deutschland", @"Schweiz", @"Österreich"] ,@"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], @"detailTextLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], FXFormFieldAction: @"countrysField"},
             
             @{FXFormFieldFooter:[NSString stringWithFormat:@"Zuletzt aktualisiert am: %@",strDate], FXFormFieldKey: @"Kennzeichen updaten", FXFormFieldAction: @"updateKennzeichen", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17]},
             
             @{FXFormFieldHeader:@"Spiel", FXFormFieldKey: @"sound", FXFormFieldTitle: @"Sound", FXFormFieldAction: @"sound", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17]},
             
             
             @{FXFormFieldHeader:@"Kontakt", FXFormFieldKey: @"rateApp", FXFormFieldTitle:@"App bewerten", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], @"detailTextLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], FXFormFieldAction: @"rateApp"},
             
             @{FXFormFieldKey: @"facebook", FXFormFieldTitle:@"Like us on Facebook", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], @"detailTextLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], FXFormFieldAction: @"facebook"},
             
             @{FXFormFieldKey: @"feedback", FXFormFieldTitle:@"Feedback senden", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], @"detailTextLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], FXFormFieldAction: @"feedback"},
            
             @{FXFormFieldKey: @"impressum", FXFormFieldTitle:@"Impressum", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], @"detailTextLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17], FXFormFieldAction: @"impressum"},

             @{FXFormFieldHeader:NSLocalizedString(@"Mit Liebe entwickelt", @"Copyright Text Header"), FXFormFieldKey: @"resetTutorial", FXFormFieldTitle: @"© 2015 - 2peaches", FXFormFieldAction: @"open2peaches", @"textLabel.color": [UIColor blackColor], @"textLabel.font": [UIFont fontWithName:@"HelveticaNeue-Light" size:17]},

             ];
    
}



@end
