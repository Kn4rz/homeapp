//
//  SettingsOptionViewController.h
//  Favorite Contacts
//
//  Created by Tim on 01.09.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FXForms/FXForms.h>

@interface SettingsOptionViewController : FXFormViewController

@end
