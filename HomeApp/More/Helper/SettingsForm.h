//
//  SettingsForm.h
//  Favorite Contacts
//
//  Created by Tim on 01.09.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForms.h"

@interface SettingsForm : NSObject <FXForm>

typedef NS_ENUM(NSInteger, Countrys)
{
    Deutschland = 0,
    Schweiz = 1,
    Oesterreich = 2
    
};

@property (nonatomic, assign) Countrys countrys;
@property (nonatomic, assign) BOOL useKeyboard;
@property (nonatomic, assign) BOOL sound;


@end
