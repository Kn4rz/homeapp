//
//  SettingsCell.h
//  Favorite Contacts
//
//  Created by Tim on 01.09.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import "FXForms.h"

@interface SettingsCell : FXFormBaseCell <FXFormFieldCell>

@end