//
//  SettingsViewController.h
//  Favorite Contacts
//
//  Created by Alex Keller on 28.08.14.
//  Copyright (c) 2014 2peaches. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FXForms/FXForms.h>
#import <iAd/iAd.h>
#import "SettingsForm.h"
#import <MessageUI/MessageUI.h>


@interface SettingsViewController :  UIViewController <FXFormControllerDelegate, MFMailComposeViewControllerDelegate> {
    
    SettingsForm *sf;

}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) FXFormController *formController;

- (void)open2peaches;
- (void)updateKennzeichen;
- (void)rateApp;
- (void)facebook;
- (void)feedback;
- (void)dismissVC;
- (NSDictionary *)countrysField;
@end
