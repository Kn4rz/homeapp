//
//  ImpressumViewController.m
//  Kennzeichen-Finder 2016
//
//  Created by Alex Keller on 25.11.15.
//  Copyright © 2015 2peaches. All rights reserved.
//

#import "ImpressumViewController.h"
#import <KINWebBrowser/KINWebBrowserViewController.h>

@interface ImpressumViewController ()

@end

@implementation ImpressumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Impressum";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnWebsite:(UIButton *)sender; {
    
    KINWebBrowserViewController *webViewController = [[KINWebBrowserViewController alloc] init];
    webViewController.barTintColor = UIColorFromRGB(0x0958cf);
    webViewController.parentViewController.view.backgroundColor = [UIColor whiteColor];
    webViewController.tintColor = [UIColor whiteColor];
    webViewController.showsURLInNavigationBar = NO;
    [webViewController loadURLString:@"http://www.2peaches.de"];
    
    [self.navigationController pushViewController:webViewController animated:YES];

    
    
}
@end
