//
//  ImpressumViewController.h
//  Kennzeichen-Finder 2016
//
//  Created by Alex Keller on 25.11.15.
//  Copyright © 2015 2peaches. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImpressumViewController : UIViewController
- (IBAction)btnWebsite:(UIButton *)sender;

@end
