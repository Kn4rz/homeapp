//
//  EditEventTriggerViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitEnums.h"
#import "EditTriggerViewController.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

typedef NS_ENUM(NSInteger, HomeKitConditionType) {
    
    CharacteristicType = 0,
    
    SunTimeType = 1,
    
    ExactTimeType = 2,
    
    UnknownType = 3
};


@interface EditEventTriggerViewController : EditTriggerViewController


@property (nonatomic, strong) HMEventTrigger *eventTrigger;
@property (nonatomic, strong) NSMutableArray *conditions;


#pragma mark - New Event Trigger creation

-(HMEventTrigger*)newTrigger;
-(NSArray*)createEventsForEventTrigger;


#pragma mark - Event Trigger update methods

-(void)updateEventTriggerConditions:(HMEventTrigger*)trigger;


#pragma mark - TableView datasource Helper

-(int)sectionForConditions;
-(NSString*)titleForAddRowInSection:(NSIndexPath*)indexPath;
-(UITableViewCell*)addCellForRowAtIndexPath:(NSIndexPath*)indexPath;
-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath;
-(NSObject*) objectsForSection:(NSInteger)section;

@end
