//
//  EditRoomViewController
//  HomeApp
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TableViewNameCell.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditRoomViewController : UITableViewController <HMAccessoryDelegate, TableViewNameCellDelegate>


@property (nonatomic, strong) HMRoom *room;


@end

