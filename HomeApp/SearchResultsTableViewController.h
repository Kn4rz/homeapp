//
//  SearchResultsTableViewController.h
//  Sample-UISearchController
//
//  Created by James Dempsey on 9/20/14.
//  Copyright (c) 2014 Tapas Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@protocol SearchResultsTableViewControllerDelegate <NSObject>
-(void) didSelectSearchResultAtIndexPath:(NSIndexPath*)indexPath;
@end

@interface SearchResultsTableViewController : UITableViewController

@property (nonatomic, strong) MKLocalSearchResponse *searchResults; // Filtered search results

@property (nonatomic, weak) id<SearchResultsTableViewControllerDelegate> _delegate;

@end
