//
//  StatePair.m
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "SunStatePair.h"
#import "TimeConditionViewController.h"

@implementation SunStatePair

-(id)init:(TimeConditionOrder)timeOrder withSunState:(TimeConditionSunState)sunState {
    
    self.timeOrder = timeOrder;
    self.sunState = sunState;
    
    return self;
}


@end
