//
//  CharacteristicStringFormatter.h
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <HomeKit/HomeKit.h>

@interface CharacteristicStringFormatter : NSObject

+(NSString*)localizedStringFromCharacteristic:(HMCharacteristic*)characteristic withValue:(id)value;
+(NSString*)localizedStringForCharacteristicValue:(id)value withCharacteristic:(HMCharacteristic*)characteristic;
+(NSString*)localizedStringForCharacteristicType:(HMCharacteristic*)characteristic;
+(NSString*)predeterminedValueDescriptionForNumber:(HMCharacteristic*)characteristic;

@end
