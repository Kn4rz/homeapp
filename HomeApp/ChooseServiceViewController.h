//
//  ChooseServiceViewController.h
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface ChooseServiceViewController : UITableViewController <HMAccessoryDelegate>

@property (strong, nonatomic) HMAccessory *selectedAccessory;


@property (nonatomic, assign) bool onlyShowsControlServices;
@property (nonatomic, assign) bool isCondition;


@end
