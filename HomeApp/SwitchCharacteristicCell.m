//
//  SwitchCharacteristicCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "SwitchCharacteristicCell.h"

@implementation SwitchCharacteristicCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)didChangeSwitchValue:(id)sender {
    if ([self.characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetLockMechanismState] ||
        [self.characteristic.characteristicType isEqualToString:HMCharacteristicTypePowerState]) {
        
        BOOL changedLockState = ![self.characteristic.value boolValue];
        
        [self.characteristic writeValue:[NSNumber numberWithBool:changedLockState] completionHandler:^(NSError *error){
            
            if(error == nil){
                
                dispatch_async(dispatch_get_main_queue(), ^(void){                    
                    [self updateValue:self.characteristic.value];
                });
            } else {
                NSLog(@"error in writing characterstic: %@",error);
            }
        }];
    }
}


@end
