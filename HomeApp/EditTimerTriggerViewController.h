//
//  EditTimerTriggerViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditTriggerViewController.h"
#import "TableViewDatePickerCell.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditTimerTriggerViewController : EditTriggerViewController <TableViewDatePickerCellDelegate>

@end
