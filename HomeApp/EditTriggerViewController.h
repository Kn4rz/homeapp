//
//  EditTriggerViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TableViewNameCell.h"
#import "TableViewSwitchCell.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

typedef NS_ENUM(NSInteger, TriggerTableViewSection) {
    // All Triggers
    Name = 0, Enabled = 1, ActionSets = 2,
    
    // Only Timer Trigger
    DateAndTime = 3, Recurrence = 4,
    
    // Location and Characteristic Trigger
    Conditions = 5,
    
    // Only Location Trigger
    Location = 6, Region = 7,
    
    // Only Characteristic Trigger
    Characteristic = 8,
    
    // Default
    Unknown = 9
};

@interface EditTriggerViewController : UITableViewController <TableViewNameCellDelegate, TableViewSwitchCellDelegate>

@property (nonatomic, strong) UIBarButtonItem *saveButton;
@property (nonatomic, strong) UITextField *nameField;
@property (nonatomic, strong) UISwitch *enabledSwitch;

@property (nonatomic, strong) HMTrigger *trigger;
@property (nonatomic, strong) NSMutableArray *actionSets;
@property (nonatomic, strong) NSMutableArray *selectedActionSets;

@property (nonatomic, assign) bool isAdding;


#pragma mark - Loading Data

-(void)loadData;
-(NSArray*)selectableActionSets;


#pragma mark - Trigger update methods

-(void)updateTriggerName:name;
-(void)enableTrigger:(HMTrigger*)trigger;
-(void)removeAllActionSetsFromTrigger:(HMTrigger*)trigger;
-(void)addAllSelectedActionSetsToTrigger:(HMTrigger*)trigger;


#pragma mark - TableView datasource Helper

-(TriggerTableViewSection)sectionForIndex:(NSInteger)section;

@end
