//
//  HomeDetailViewController
//  HomeApp
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitEnums.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface HomeDetailViewController : UITableViewController <HMAccessoryDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>


@property (nonatomic, strong) HMAccessory *activeAccessory;
@property (nonatomic, strong) HMRoom *activeRoom;


@end

