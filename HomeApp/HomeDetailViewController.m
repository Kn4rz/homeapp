//
//  SecondViewController.m
//  HomeKit_KnarzSample
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeDetailViewController.h"
#import "AddNewAccesoryViewController.h"
#import "EditRoomViewController.h"
#import "EditAccessoryViewController.h"
#import "CharacteristicsViewController.h"
#import "ServicesViewController.h"
#import "EditZoneViewController.h"
#import "EditSceneViewController.h"
#import "EditTimerTriggerViewController.h"
#import "EditLocationTriggerViewController.h"
#import "HomeKitManager.h"

@interface HomeDetailViewController () {
    NSMutableArray *accessories;
    NSMutableArray *rooms;
    NSMutableArray *zones;
    NSMutableArray *actionSets;
    NSMutableArray *triggers;
}

@end

@implementation HomeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = [HomeKitManager sharedManager].currentHome.name;
    
    UIBarButtonItem *renameButton = [[UIBarButtonItem alloc] init];
    renameButton.title = @"Rename";
    [renameButton setAction:@selector(renameButtonPressed)];
    [renameButton setTarget:self];
    self.navigationItem.rightBarButtonItem = renameButton;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadData {
    accessories = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.accessories];
    
    rooms = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.rooms];
    [rooms addObject:[[HomeKitManager sharedManager] getRoomForEntireHome]];
    
    zones = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.zones];
    
    actionSets = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.actionSets];
    
    triggers = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.triggers];
    
    [self.tableView reloadData];
}

-(void)renameButtonPressed {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Rename Home" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
        
        [[HomeKitManager sharedManager] renameHome:textField.text toHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error) {
            if (error) {
                NSLog(@"Error renaming home: %@", error);
            } else {
                NSLog(@"Home name renamed.");
                self.title = [HomeKitManager sharedManager].currentHome.name;
            }
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)showNewRoomView {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add New Room" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
                
        [[HomeKitManager sharedManager] addRoom:textField.text toHome:[HomeKitManager sharedManager].currentHome completion:^(HMRoom *room, NSError *error) {
            if (error) {
                NSLog(@"Error adding new room: %@", error);
            } else {
                NSLog(@"New room added.");
                [self addRoom:room];
            }
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)showNewZoneView {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add New Zone" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
        
        [[HomeKitManager sharedManager] addZone:textField.text toHome:[HomeKitManager sharedManager].currentHome completion:^(HMZone *zone, NSError *error) {
            if (error) {
                NSLog(@"Error adding new zone: %@", error);
            } else {
                NSLog(@"New zone added.");
                [self addZone:zone];
            }
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)addRoom:(HMRoom*)room {
    NSMutableArray *newRooms = [NSMutableArray arrayWithArray:rooms];
    [newRooms addObject:room];
    rooms = [NSMutableArray arrayWithArray:newRooms];

    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:Room];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)addZone:(HMZone*)zone {
    NSMutableArray *newZones = [NSMutableArray arrayWithArray:zones];
    [newZones addObject:zone];
    zones = [NSMutableArray arrayWithArray:newZones];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:Zone];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)createTrigger {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Add Trigger" preferredStyle:UIAlertControllerStyleActionSheet];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Time" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Add Timer Trigger" sender:self];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Add Location Trigger" sender:self];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(bool)isLocationTrigger:(HMEventTrigger*)trigger {
    for (HMEvent *event in trigger.events) {
        if([event isKindOfClass:[HMLocationEvent class]]) {
            return true;
        }
    }
    return false;
}

-(void)deleteAccessory:(HMAccessory*)accessory {
    [[HomeKitManager sharedManager] removeAccessory:accessory fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Accessory from Home");
        
        [accessories removeObject:accessory];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:Accessory];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

-(void)deleteRoom:(HMRoom*)room {
    [[HomeKitManager sharedManager] removeRoom:room fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Room from Home");
        
        [rooms removeObject:room];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:Room];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

-(void)deleteZone:(HMZone*)zone {
    [[HomeKitManager sharedManager] removeZone:zone fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Zone from Home");
        
        [zones removeObject:zone];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:Zone];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

-(void)deleteActionSet:(HMActionSet*)actionSet {
    [[HomeKitManager sharedManager] removeActionSet:actionSet fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Scene from Home");
        
        [actionSets removeObject:actionSet];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:ActionSet];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

-(void)deleteTrigger:(HMTrigger*)trigger {
    [[HomeKitManager sharedManager] removeTrigger:trigger fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Trigger from Home");
        
        [triggers removeObject:trigger];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:Trigger];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case Accessory:
            return @"Accesories";
            
        case Room:
            return @"Rooms";
            
        case Zone:
            return @"Zones";
            
        case ActionSet:
            return @"Scenes";
            
        case Trigger:
            return @"Triggers";
            
        default:
            return @"undefined";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case Accessory:
            return accessories.count + 1;
            
        case Room:
            return rooms.count + 1;
            
        case Zone:
            return zones.count + 1;
            
        case ActionSet:
            return actionSets.count + 1;
       
        case Trigger:
            return triggers.count + 1;
            
        default:
            return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self cellTypeForIndexPath:indexPath]) {
        case Add:
            return [self addCellForRowAtIndexPath:indexPath];
        
        case Object:
            return [self homeKitObjectCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
 
        case Room:
            if(indexPath.row != rooms.count) {
                return !([[[HomeKitManager sharedManager] getRoomForEntireHome] isEqual:[rooms objectAtIndex:indexPath.row]] || [self cellTypeForIndexPath:indexPath] == Add);
            }
            return !([self cellTypeForIndexPath:indexPath] == Add);
            
        case ActionSet:
            
            if(indexPath.row != actionSets.count) {
                HMActionSet *actionSet = [actionSets objectAtIndex:indexPath.row];
                bool isBuiltInType = ![actionSet.actionSetType isEqualToString:HMActionSetTypeUserDefined];
                
                return !(isBuiltInType || [self cellTypeForIndexPath:indexPath] == Add);
            }
            return !([self cellTypeForIndexPath:indexPath] == Add);
            
        default:
            return !([self cellTypeForIndexPath:indexPath] == Add);
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        switch (indexPath.section) {
            case Accessory:
                [self deleteAccessory:[accessories objectAtIndex:indexPath.row]];
                break;
                
            case Room:
                [self deleteRoom:[rooms objectAtIndex:indexPath.row]];
                break;
                
            case Zone:
                [self deleteZone:[zones objectAtIndex:indexPath.row]];
                break;
                
            case ActionSet:
                [self deleteActionSet:[actionSets objectAtIndex:indexPath.row]];
                break;
                
            case Trigger:
                [self deleteTrigger:[triggers objectAtIndex:indexPath.row]];
                break;
             
            default:
                break;
        }
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        switch (indexPath.section) {
            case Accessory:
                [self performSegueWithIdentifier:@"AddAccessory" sender:self];
                break;
                
            case Room:
                [self showNewRoomView];
                break;
            
            case Zone:
                [self showNewZoneView];
                break;
                
            case ActionSet:
                [self performSegueWithIdentifier:@"Add Action Set" sender:self];
                break;
                
            case Trigger:
                [self createTrigger];
                break;
        }
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == Trigger) {
        
        HMTrigger *trigger = [triggers objectAtIndex:indexPath.row];
        if([trigger isKindOfClass:[HMTimerTrigger class]]) {
            
            [self performSegueWithIdentifier:@"Show Timer Trigger" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
            
        } else {
            
            if([self isLocationTrigger:(HMEventTrigger*)trigger]) {
                [self performSegueWithIdentifier:@"Show Location Trigger" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
            
            } else {
                [self performSegueWithIdentifier:@"Show Characterisitic Trigger" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
            }
            
        }
    }
}


#pragma mark - TableView datasource - Helper

-(NSString*)titleForAddRowInSection:(NSIndexPath*)indexPath {
    switch (indexPath.section) {
        case Accessory:
            return @"Add Accessory…";
            
        case Room:
            return @"Add Room…";
            
        case Zone:
            return @"Add Zone…";
            
        case ActionSet:
            return @"Add Scene…";
            
        case Trigger:
            return @"Add Trigger…";
            
        default:
            return nil;
    }
}

-(UITableViewCell*)addCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    NSString *reuseIdentifier = @"AddCell";
    
    UITableViewCell *cell =  [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = [self titleForAddRowInSection:indexPath];
    return cell;
}

-(UITableViewCell*)homeKitObjectCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    NSString *name;
    if (indexPath.section == 0) {
        HMAccessory *accessory = [accessories objectAtIndex:indexPath.row];
        name = accessory.name;
    }
    if (indexPath.section == 1) {
        HMRoom *room = [rooms objectAtIndex:indexPath.row];
        name = [room isEqual:[[HomeKitManager sharedManager] getRoomForEntireHome]] ? [NSString stringWithFormat:@"%@ (Default Room)",room.name] : room.name;
    }
    if (indexPath.section == 2) {
        HMZone *zone = [zones objectAtIndex:indexPath.row];
        name = zone.name;
    }
    if (indexPath.section == 3) {
        HMActionSet *actionSet = [actionSets objectAtIndex:indexPath.row];
        name = actionSet.name;
    }
    if (indexPath.section == 4) {
        HMTrigger *trigger = [triggers objectAtIndex:indexPath.row];
        name = trigger.name;
    }
    
    NSString *reuseIdentifier;
    if (indexPath.section == 0) {
        HMAccessory *accessory = [accessories objectAtIndex:indexPath.row];
        reuseIdentifier = accessory.reachable ? @"AccessoryCell" : @"UnreachableAccessoryCell";
    }
    if (indexPath.section == 1) {
        reuseIdentifier = @"RoomCell";
    }
    if (indexPath.section == 2) {
        reuseIdentifier = @"ZoneCell";
    }
    if (indexPath.section == 3) {
        reuseIdentifier = @"ActionSetCell";
    }
    if (indexPath.section == 4) {
        reuseIdentifier = @"TriggerCell";
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = name;
    return cell;
}

-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath {
    
    NSInteger objectCount = [(NSArray*)[self objectsForSection:indexPath.section] count];
    if (indexPath.row == objectCount) {
        return Add;
    }
    else {
        return Object;
    }
    
}

-(NSObject*) objectsForSection:(HomeKitObjectSection)section {
    switch (section) {
            
        case Accessory:
            return accessories;
            
        case Room:
            return rooms;
            
        case Zone:
            return zones;
            
        case ActionSet:
            return actionSets;
        
        case Trigger:
            return triggers;
            
        default:
            return [[NSMutableArray alloc] init];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Show Accessory"]) {
            ServicesViewController *avc = (ServicesViewController*)[segue destinationViewController];
            avc.selectedAccessory = [accessories objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Modify Accessory"]) {
            EditAccessoryViewController *eavc = (EditAccessoryViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            eavc.accessory = [accessories objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Show Room"]) {
            EditRoomViewController *rvc = (EditRoomViewController*)segue.destinationViewController;
            rvc.room = [rooms objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Show Zone"]) {
            EditZoneViewController *zvc = (EditZoneViewController*)[segue destinationViewController];
            zvc.selectedZone = [zones objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Show Action Set"]) {
            EditSceneViewController *svc = (EditSceneViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            svc.actionSet = [actionSets objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Show Timer Trigger"]) {
            EditTimerTriggerViewController *svc = (EditTimerTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            svc.trigger = [triggers objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Show Location Trigger"]) {
            EditLocationTriggerViewController *vc = (EditLocationTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            vc.trigger = [triggers objectAtIndex:indexPath.row];
        }
        
    } else {
        
        if ([segue.identifier isEqualToString:@"Add Action Set"]) {
            EditSceneViewController *svc = (EditSceneViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            svc.isAdding = true;
        }
        
        if ([segue.identifier isEqualToString:@"Add Timer Trigger"]) {
            EditTimerTriggerViewController *svc = (EditTimerTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            svc.isAdding = true;
        }
        
        if ([segue.identifier isEqualToString:@"Add Location Trigger"]) {
            EditLocationTriggerViewController *vc = (EditLocationTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            vc.isAdding = true;
        }
    }
}

@end
