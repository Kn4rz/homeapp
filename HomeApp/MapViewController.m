//
//  MapViewControlle.r.m
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "MapViewController.h"
#import "DBMapSelectorManager.h"
#import "SearchResultsTableViewController.h"
#import <MapKit/MapKit.h>

@interface MapViewController () <UISearchBarDelegate, DBMapSelectorManagerDelegate, SearchResultsTableViewControllerDelegate> {
    CLLocationManager *locationManager;
    
    CLLocationDistance distance;
    
    
    CGRect _searchTableViewRect;
}

// Search
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) MKLocalSearch *localSearch;
@property (strong, nonatomic) MKLocalSearchResponse *results;

@property (nonatomic, strong) DBMapSelectorManager *mapSelectorManager;

@end


@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Keep the subviews inside the top and bottom layout guides
    self.edgesForExtendedLayout = UIRectEdgeLeft | UIRectEdgeBottom | UIRectEdgeRight;
    // Fix black glow on navigation bar
    [self.navigationController.view setBackgroundColor:[UIColor whiteColor]];
    
    
    UITapGestureRecognizer *tpgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapPress:)];
    [self.mapView addGestureRecognizer:tpgr];
    
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    
    // Set map selector settings
    self.mapSelectorManager = [[DBMapSelectorManager alloc] initWithMapView:self.mapView];
    self.mapSelectorManager.delegate = self;
    self.mapSelectorManager.circleRadiusMin = 25;
    self.mapSelectorManager.circleRadius = 25;
   
    
    // The table view controller is in a nav controller, and so the containing nav controller is the 'search results controller'
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *searchResultsController = [storyboard instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
    SearchResultsTableViewController *vc = (SearchResultsTableViewController *)searchResultsController.topViewController;
    vc._delegate = self;
    
    // Initialize our UISearchController
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchBar.delegate = self;
    [self setupSearchBar];
    self.definesPresentationContext = YES;
    
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestLocation];
    
    if(self.targetRegion != nil) {
        [self annotateAndZoomToRegion:self.targetRegion];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) setupSearchBar {
    
    // Set search bar dimension and position
    CGRect searchBarFrame = self.searchController.searchBar.frame;
    CGRect viewFrame = self.view.frame;
    self.searchController.searchBar.frame = CGRectMake(searchBarFrame.origin.x,
                                                       searchBarFrame.origin.y,
                                                       viewFrame.size.width,
                                                       44.0);
    
    // Add SearchController's search bar to our view and bring it to front
    [self.view addSubview:self.searchController.searchBar];
    [self.view bringSubviewToFront:self.searchController.searchBar];
}

- (void)handleTapPress:(UIGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)return;
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchCoordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    NSLog(@"%f, %f", touchCoordinate.latitude, touchCoordinate.longitude);
    
    self.mapSelectorManager.circleCoordinate = touchCoordinate;
    [self.mapSelectorManager applySelectorSettings];
}

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveButtonPressed {
    
    CLCircularRegion *genericRegion = [[CLCircularRegion alloc] initWithCenter:self.mapView.region.center radius:distance identifier:@"MapViewController.Region"];
    
    [self._delegate mapViewDidUpdateRegion:genericRegion];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)annotateAndZoomToRegion:(CLCircularRegion*)region {
    MKCoordinateRegion mapRegion = MKCoordinateRegionMakeWithDistance(region.center, region.radius, region.radius);
    [self.mapView setRegion:mapRegion animated:YES];
    
    self.mapSelectorManager.circleCoordinate = CLLocationCoordinate2DMake(region.center.latitude, region.center.longitude);
    self.mapSelectorManager.circleRadius = region.radius;
    [self.mapSelectorManager applySelectorSettings];
}


#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (![searchText isEqualToString:@""]) {
        [self searchQuery:searchText];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar {
    [self searchQuery:aSearchBar.text];
}

- (void)searchQuery:(NSString *)query {
    // Cancel any previous searches.
    [self.localSearch cancel];
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = query;
    request.region = self.mapView.region;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (error != nil) {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Map Error" message:[error description] preferredStyle:UIAlertControllerStyleAlert];
            
            [alertView addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertView animated:YES completion:nil];

            return;
        }
        
        //			if ([response.mapItems count] == 0) {
        //				[[[UIAlertView alloc] initWithTitle:@"No Results"
        //											message:nil
        //										   delegate:nil
        //								  cancelButtonTitle:@"OK"
        //								  otherButtonTitles:nil] show];
        //				return;
        //			}
        
        self.results = response;
        
        if (self.searchController.searchResultsController) {
            UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
            
            SearchResultsTableViewController *vc = (SearchResultsTableViewController *)navController.topViewController;
            vc.searchResults = self.results;
            [vc.tableView reloadData];
        }
        
    }];
    

}

-(void)willPresentSearchController:(UISearchController *)aSearchController {
    
    aSearchController.searchBar.bounds = CGRectInset(aSearchController.searchBar.frame, 0.0f, 0.0f);
    
    // Set the position of the result's table view below the status bar and search bar
    // Use of instance variable to do it only once, otherwise it goes down at every search request
    if (CGRectIsEmpty(_searchTableViewRect)) {
        CGRect tableViewFrame = ((UITableViewController *)aSearchController.searchResultsController).tableView
        .frame;
        tableViewFrame.origin.y = tableViewFrame.origin.y + 64; //status bar (20) + nav bar (44)
        tableViewFrame.size.height =  tableViewFrame.size.height;
        
        _searchTableViewRect = tableViewFrame;
    }
    
    [((UITableViewController *)aSearchController.searchResultsController).tableView setFrame:_searchTableViewRect];
}


#pragma mark - DBMapSelectorManager Delegate

- (void)mapSelectorManager:(DBMapSelectorManager *)mapSelectorManager didChangeCoordinate:(CLLocationCoordinate2D)coordinate {
    
}

- (void)mapSelectorManager:(DBMapSelectorManager *)mapSelectorManager didChangeRadius:(CLLocationDistance)radius {
    distance = radius;
    self.mapSelectorManager.circleRadius = radius;
}


#pragma mark - SearchResultsTableViewController Delegate

-(void) didSelectSearchResultAtIndexPath:(NSIndexPath*)indexPath {
    // Hide search controller
    [self.searchController setActive:NO];
    
    MKMapItem *item = self.results.mapItems[indexPath.row];
    
    NSLog(@"Selected \"%@\"", item.placemark.name);
    NSLog(@"Coordinates: %f %f", item.placemark.location.coordinate.latitude, item.placemark.location.coordinate.longitude);
    
//    [self.mapView addAnnotation:item.placemark];
//    [self.mapView selectAnnotation:item.placemark animated:YES];
    
//    [self.mapView setCenterCoordinate:item.placemark.location.coordinate animated:YES];
//    [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
    
    self.mapSelectorManager.circleCoordinate = item.placemark.location.coordinate;
    [self.mapSelectorManager applySelectorSettings];
}


#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    return [self.mapSelectorManager mapView:mapView viewForAnnotation:annotation];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    [self.mapSelectorManager mapView:mapView annotationView:annotationView didChangeDragState:newState fromOldState:oldState];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
    return [self.mapSelectorManager mapView:mapView rendererForOverlay:overlay];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [self.mapSelectorManager mapView:mapView regionDidChangeAnimated:animated];
}


#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
  
    if(self.targetRegion != nil) {
        return;
    }
    CLLocation *lastLocation = locations.lastObject;
    MKCoordinateRegion newRegion = MKCoordinateRegionMake(lastLocation.coordinate, MKCoordinateSpanMake(0.0015, 0.0015));
    [self.mapView setRegion:newRegion animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"System: Location Manager Error: (%@)", error);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [locationManager requestLocation];
}


@end
