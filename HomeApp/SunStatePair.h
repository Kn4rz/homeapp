//
//  StatePair.h
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeConditionViewController.h"

@interface SunStatePair : NSObject


@property (nonatomic, assign) TimeConditionOrder timeOrder;
@property (nonatomic, assign) TimeConditionSunState sunState;



-(id)init:(TimeConditionOrder)timeOrder withSunState:(TimeConditionSunState)sunState;

@end
