//
//  ChooseCharacteristicViewController.m
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import "AppDelegate.h"
#import "ChooseCharacteristicViewController.h"
#import "AddCharacteristicValueViewController.h"

@interface ChooseCharacteristicViewController () {
    NSMutableArray *charateristics;
    NSNumber *value;
}

@end

@implementation ChooseCharacteristicViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.selectedService.name;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] init];
    doneButton.title = @"Done";
    [doneButton setAction:@selector(cancelButtonPressed)];
    [doneButton setTarget:self];
    self.navigationItem.rightBarButtonItem = doneButton;

    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.selectedService) {
        [self loadData];
    }
}


#pragma mark - Loading Data

-(void) loadData {
    self.selectedService.accessory.delegate = self;
    charateristics = [[NSMutableArray alloc] initWithArray:[self filterCharateristics]];
    [self.tableView reloadData];
}

-(NSArray*)filterCharateristics {
    NSMutableArray *filteredCharateristics = [[NSMutableArray alloc] init];
    
    for(HMCharacteristic *charateristic in self.selectedService.characteristics) {
        
        if([charateristic.properties containsObject:HMCharacteristicPropertyWritable]) {
            [filteredCharateristics addObject:charateristic];
        }
    }
    return filteredCharateristics;
}


#pragma mark - Button Actions

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateServices:(HMAccessory *)accessory {
    [self loadData];
}

- (void)accessory:(HMAccessory *)accessory service:(HMService *)service didUpdateValueForCharacteristic:(HMCharacteristic *)characteristic {
    [self loadData];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Choose Characteristic";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return charateristics.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HMCharacteristic *characteristic = [charateristics objectAtIndex:indexPath.row];
//    HMAccessory *accessory = self.selectedService.accessory;
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CharacteristicCell"];
    cell.textLabel.text = characteristic.localizedDescription;
//    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ in %@", self.selectedService.name, accessory.name];

//    if ([characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetLockMechanismState] || [characteristic.characteristicType isEqualToString:HMCharacteristicTypePowerState])
//    {
//        
//        BOOL lockState = [characteristic.value boolValue];
//        
//        UISwitch *lockSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
//        lockSwitch.on = lockState;
//        [lockSwitch addTarget:self action:@selector(changeLockState:) forControlEvents:UIControlEventValueChanged];
//        cell.accessoryView = lockSwitch;
//        
//    } else if ([characteristic.characteristicType isEqualToString:HMCharacteristicTypeSaturation] ||
//               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeBrightness] ||
//               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeHue] ||
//               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetTemperature] ||
//               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetRelativeHumidity] ||
//               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeCoolingThreshold] ||
//               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeHeatingThreshold])
//    {
//        UISlider *slider = [[UISlider alloc] init];
//        slider.bounds = CGRectMake(0, 0, 125, slider.bounds.size.height);
//        slider.maximumValue = [characteristic.metadata.maximumValue floatValue];
//        slider.minimumValue = [characteristic.metadata.minimumValue floatValue];
//        slider.value = [characteristic.value floatValue];
//        slider.continuous = true;
//        [slider addTarget:self action:@selector(changeSliderValue:) forControlEvents:UIControlEventValueChanged];
//        
//        cell.accessoryView = slider;
//    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"Add Value" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Add Value"]) {
            AddCharacteristicValueViewController *acvc = (AddCharacteristicValueViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            acvc.selectedCharacteristic = [charateristics objectAtIndex:indexPath.row];
            acvc.isAdding = true;
            acvc.isCondition = self.isCondition;
        }
    }
}

@end
