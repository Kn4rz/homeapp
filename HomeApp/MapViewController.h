//
//  MapViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol MapViewControllerDelegate <NSObject>
-(void) mapViewDidUpdateRegion:(CLCircularRegion*)region;
@end


@interface MapViewController : UIViewController  <CLLocationManagerDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) id<MapViewControllerDelegate> _delegate;
@property (nonatomic, strong) CLCircularRegion* targetRegion;

@end