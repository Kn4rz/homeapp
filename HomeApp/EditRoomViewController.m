//
//  EditRoomViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 02.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditRoomViewController.h"
#import "EditAccessoryViewController.h"
#import "TableViewNameCell.h"
#import "HomeKitManager.h"

@interface EditRoomViewController () {
    UIBarButtonItem *saveButton;
    UITextField *_nameField;
    
    NSMutableArray *accessories;
}
@end


@implementation EditRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = self.room.name;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NameCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NameCell"];
    
    saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    saveButton.enabled = NO;
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    accessories = [[NSMutableArray alloc] init];
    for(HMAccessory *accessory in self.room.accessories) {
        accessory.delegate = self;
        [accessories addObject:accessory];
    }
    [self.tableView reloadData];
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    saveButton.enabled = trimmedName.length > 0 && ![self.room.name isEqualToString:trimmedName];
}

-(void)saveButtonPressed {
    
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    [[HomeKitManager sharedManager] renameRoom:trimmedName inRoom:self.room completion:^(NSError *error) {
        if (error) {
            NSLog(@"Error renaming room: %@", error);
        } else {
            NSLog(@"Room renamed");
            self.title = self.room.name;
            if ([_nameField isFirstResponder]) {
                [_nameField resignFirstResponder];
            }
        }
    }];
}


#pragma mark - TableViewNameCell Delegate

-(void)nameFieldDidChanged {
    [self enableSaveButtonIfNecessary];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateReachability:(HMAccessory *)accessory {
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:1];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return nil;
    }
    
    if (accessories == nil || accessories.count == 0) {
        return nil;
    }
    return @"Accessories";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    }
    return accessories.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    NSLog(@"accessories: %@", accessories);
    
    if(indexPath.section == 0) {
        TableViewNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
        if (cell == nil) {
            cell = [[TableViewNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
        }
        _nameField = cell.nameField;
        cell._delegate = self;
        
        cell.nameField.text = self.room.name;
        cell.nameField.enabled = ![[[HomeKitManager sharedManager] getRoomForEntireHome] isEqual:self.room];
        return cell;
    }
    
    HMAccessory *accessory = [accessories objectAtIndex:indexPath.row];
    NSString *reuseIdentifier = accessory.reachable ? @"AccessoryCell" : @"UnreachableAccessoryCell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = accessory.name;

    return cell;
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if ([segue.identifier isEqualToString:@"Modify Accessory"]) {
            EditAccessoryViewController *avc = (EditAccessoryViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            avc.accessory = [accessories objectAtIndex:indexPath.row];
        }
        
    }
}

@end
