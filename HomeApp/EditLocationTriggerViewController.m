//
//  EditLocationTriggerViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditLocationTriggerViewController.h"
#import "MapViewController.h"
#import "HomeKitManager.h"
#import <HomeKit/HomeKit.h>

@import Contacts;


@interface EditLocationTriggerViewController () {
    
    CLGeocoder *geocoder;
    
    UIBarButtonItem *saveButton;
    UITextField *_nameField;
    UISwitch *_enabledSwitch;
    
    CLCircularRegion* targetRegion;
    
    HMLocationEvent *locationEvent;
    NSMutableArray *regionStatusTitles;
    NSMutableArray *conditions;
    
    int selectedRegionStatus;
    
    NSString *localizedString;
}
@end


@implementation EditLocationTriggerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    geocoder = [[CLGeocoder alloc] init];
    
    regionStatusTitles = [[NSMutableArray alloc] initWithObjects:@"When I Enter The Area", @"When I Leave The Area", nil];
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateLocalizedAdressString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    [super loadData];
    
    locationEvent = [self getLocationEventFromTrigger:self.eventTrigger];
    
    targetRegion = self.isAdding ? nil : (CLCircularRegion*)locationEvent.region;
    
    selectedRegionStatus = self.isAdding ? 0 : [self getRegionStatus:locationEvent];
    
    [self.tableView reloadData];
}

-(HMLocationEvent*)getLocationEventFromTrigger:(HMEventTrigger*)trigger {
    for (HMEvent *event in trigger.events) {
        if([event isKindOfClass:[HMLocationEvent class]]) {
            return (HMLocationEvent*)event;
        }
    }
    return nil;
}

-(int)getRegionStatus:(HMLocationEvent*)event {
    if(event.region.notifyOnEntry) {
        return 0;
    }
    if(event.region.notifyOnExit) {
        return 1;
    }
    return 0;
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    
    NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    if(self.isAdding) {
        self.saveButton.enabled = (trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName] && targetRegion != nil);
        
    } else {
        self.saveButton.enabled = true;
    }
}

-(void)saveButtonPressed {
    
    if(self.isAdding) {
        
        HMEventTrigger *trigger = [self newTrigger];
        [[HomeKitManager sharedManager] addTrigger:trigger toHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error) {
            
            if(error) {
                NSLog(@"Error adding location trigger %@", error);
                
            } else {
                
                [self addAllSelectedActionSetsToTrigger:trigger];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self enableTrigger:trigger];
                });
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
        
    } else {
        
        NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        if(trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName])
            [self updateTriggerName:trimmedName];
        
        
        if([locationEvent.region isEqual:targetRegion])
            [self updateLocationInTrigger:self.trigger];
        
        
        if([self getRegionStatus:locationEvent] != selectedRegionStatus)
            [self updateLocationInTrigger:self.trigger];
        
        
        [self updateEventTriggerConditions:self.eventTrigger];
        
        
        if((self.selectedActionSets != nil && self.selectedActionSets.count > 0)
           && self.selectedActionSets.count != self.trigger.actionSets.count) {
            
            [self removeAllActionSetsFromTrigger:self.trigger];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addAllSelectedActionSetsToTrigger:self.trigger];
            });
        }
        
        
        if(self.trigger.isEnabled != self.enabledSwitch.on)
            [self enableTrigger:self.trigger];
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - New Event Trigger creation

-(NSArray*)createEventsForEventTrigger {
    NSMutableArray *events = [[NSMutableArray alloc] init];
    
    [self prepareRegion];
    HMLocationEvent *location = [[HMLocationEvent alloc] initWithRegion:targetRegion];
    
    [events addObject:location];
    
    return events;
}

-(void)prepareRegion {
    targetRegion.notifyOnEntry = (selectedRegionStatus == 0);
    targetRegion.notifyOnExit = !targetRegion.notifyOnEntry;
}


#pragma mark - Trigger update methods

-(void)updateLocationInTrigger:(HMTrigger*)trigger {
    [self prepareRegion];
    [locationEvent updateRegion:targetRegion completionHandler:^(NSError * _Nullable error) {
        if(error) {
            
            NSLog(@"Error updating region in location trigger %@", error);
            
        } else {
            NSLog(@"Region in location trigger updated");
        }
    }];
}


#pragma mark - MapViewController Delegate

-(void) mapViewDidUpdateRegion:(CLCircularRegion*)region {
    targetRegion = region;
    [self enableSaveButtonIfNecessary];
    [self updateLocalizedAdressString];
}

-(void)updateLocalizedAdressString {
    CLLocation *location = [[CLLocation alloc]initWithLatitude:targetRegion.center.latitude longitude:targetRegion.center.longitude];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            NSLog(@"Error %@", error.description);
            
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
            
            CNMutablePostalAddress *postalAddress = [[CNMutablePostalAddress alloc] init];
            postalAddress.street = [NSString stringWithFormat:@"%@ %@", placemark.subThoroughfare == nil ? @"" : placemark.subThoroughfare, placemark.thoroughfare == nil ? @"" : placemark.thoroughfare];
            postalAddress.city = placemark.locality == nil ? @"" : placemark.locality;
            postalAddress.state = placemark.administrativeArea == nil ? @"" : placemark.administrativeArea;
            postalAddress.postalCode = placemark.postalCode  == nil ? @"" : placemark.postalCode;
            postalAddress.country = placemark.country  == nil ? @"" : placemark.country;
            
            NSString *addressString = [CNPostalAddressFormatter stringFromPostalAddress:postalAddress style:CNPostalAddressFormatterStyleMailingAddress];
            localizedString = [addressString stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
            
            NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:2];
            [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
            
        case Location:
            return @"Location";
            
        case Region:
            return @"Region";
            
        default:
            return [super tableView:tableView titleForHeaderInSection:section];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {

        case Region:
            return regionStatusTitles.count;

        default:
            return [super tableView:tableView numberOfRowsInSection:section];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Location:
            return [self locationCellForRowAtIndexPath:indexPath];
            
        case Region:
            return [self regionStatusCellForRowAtIndexPath:indexPath];
            
        default:
            return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Location:
            [self performSegueWithIdentifier:@"Select Location" sender:self];
            break;
            
        case Region:
            [self didSelectRegeionStatus:indexPath];
            break;
            
        default:
            [super tableView:tableView didSelectRowAtIndexPath:indexPath];
            break;
    }
}


#pragma mark - TableView datasource Helper

-(int)sectionForConditions {
    return 4;
}

-(TriggerTableViewSection)sectionForIndex:(NSInteger)section {
    switch (section) {
        case 0:
            return Name;
            
        case 1:
            return Enabled;
            
        case 2:
            return Location;
            
        case 3:
            return Region;
            
        case 4:
            return Conditions;
            
        case 5:
            return ActionSets;
            
        default:
            return Unknown;
    }
}

-(UITableViewCell*)locationCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"LocationCell"];
    
    if(targetRegion != nil) {
        cell.textLabel.text = localizedString != nil ? localizedString : @"Update Location";
    } else {
        cell.textLabel.text = @"Set Location";
    }
    return cell;
}

-(UITableViewCell*)regionStatusCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"RegionStatusCell"];
    cell.textLabel.text = [regionStatusTitles objectAtIndex:indexPath.row];
    
    cell.accessoryType = indexPath.row == selectedRegionStatus ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}

-(void)didSelectRegeionStatus:(NSIndexPath*)indexPath {
    selectedRegionStatus = (int)indexPath.row;
    [self enableSaveButtonIfNecessary];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:@"Select Location"]) {
        MapViewController *mvc = (MapViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        mvc._delegate = self;
        mvc.targetRegion = targetRegion;
    
    } 
}

@end
