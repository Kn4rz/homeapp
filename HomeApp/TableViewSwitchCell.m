//
//  TableViewSwitchCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//


#import "TableViewSwitchCell.h"


@implementation TableViewSwitchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    
    
    
}

- (void)prepareForReuse {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (UITableView *)styleTableView:(UITableView *)_tableView forId:(NSString *)cellId {
    
    
    
    return _tableView;
    
}

- (IBAction)switchDidChanged:(id)sender {
    [self._delegate switchDidChanged];
}

@end
