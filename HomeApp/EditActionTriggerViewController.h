//
//  EditActionTriggerViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 11.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitEnums.h"
#import "EditEventTriggerViewController.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditActionTriggerViewController : EditEventTriggerViewController

@end
