//
//  SegmentedControlCharacteristicCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "SegmentedControlCharacteristicCell.h"
#import "CharacteristicStringFormatter.h"

@implementation SegmentedControlCharacteristicCell

-(void)setCharacteristicValue:(HMCharacteristic*)characteristic {
    [super setCharacteristicValue:characteristic];
    
    [self.segmentedControl removeAllSegments];
    
    NSArray *allPossibleValues = [self allPossibleValues];
    for (int i = 0; i < allPossibleValues.count; i++) {
        NSNumber *value = allPossibleValues[i];
        NSString *title = [CharacteristicStringFormatter localizedStringForCharacteristicValue:value withCharacteristic:self.characteristic];
        
        [self.segmentedControl insertSegmentWithTitle:title atIndex:i animated:false];
    }
    
    self.segmentedControl.selectedSegmentIndex = [characteristic.value integerValue];
}

-(NSInteger)numberOfChoices {
    
    HMCharacteristicMetadata *metadata = self.characteristic.metadata;
    NSNumber *minimumValue = metadata.minimumValue;
    NSNumber *maximumValue = metadata.maximumValue;
    
    NSNumber *range = [NSNumber numberWithInteger:([maximumValue integerValue] - [minimumValue integerValue])];
    if(metadata.stepValue != nil) {
        range = [NSNumber numberWithInteger:([range doubleValue] / [metadata.stepValue doubleValue])];
    }
    return [range integerValue] + 1;
}

-(NSArray*)allPossibleValues {
    NSMutableArray *choices = [[NSMutableArray alloc] initWithCapacity:[self numberOfChoices]];
    for(int i = 0; i < [self numberOfChoices]; i++) {
        choices[i] = [[NSNumber alloc] initWithInt:i];
    }
    return choices;
}

- (IBAction)segmentedControlDidChange:(id)sender {
    
    [self.characteristic writeValue:[NSNumber numberWithInteger:self.segmentedControl.selectedSegmentIndex] completionHandler:^(NSError *error){
        
        if(error == nil){
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self updateValue:self.characteristic.value];
            });
        } else {
            NSLog(@"error in writing characterstic: %@",error);
        }
    }];
    
}

@end
