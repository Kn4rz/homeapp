//
//  AppDelegate.h
//  HomeApp
//
//  Created by Markus Drechsel on 02.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableDictionary *HomeKitUUIDs;


@end

