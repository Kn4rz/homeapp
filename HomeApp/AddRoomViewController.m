//
//  AddRoomViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "AddRoomViewController.h"
#import "HomeKitManager.h"

@interface AddRoomViewController () {
    NSMutableArray *displayedRooms;
    NSMutableArray *selectedRooms;
}
@end

@implementation AddRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.title = self.zone.name;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] init];
    doneButton.title = @"Done";
    [doneButton setAction:@selector(doneButtonPressed)];
    [doneButton setTarget:self];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    selectedRooms = [[NSMutableArray alloc] init];
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadData {
    displayedRooms = [[NSMutableArray alloc] initWithArray:[self roomsNotAlreadyInZone:self.zone includingRoom:[HomeKitManager sharedManager].currentHome.rooms]];
    [self.tableView reloadData];
}

-(NSArray*)roomsNotAlreadyInZone:(HMZone*)zone includingRoom:(NSArray*)rooms {
    NSMutableArray *filteredRooms = [[NSMutableArray alloc] init];
    
    for(HMRoom *room in rooms) {
        if(![zone.rooms containsObject:room]) {
            [filteredRooms addObject:room];
        }
    }
    return filteredRooms;
}

-(void)doneButtonPressed {
    __weak typeof (self) weakSelf = self;
    [self addSelectedRoomsToZoneWithCompletion:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];
}

-(void)addSelectedRoomsToZoneWithCompletion:(void (^)())completion {
    for(HMRoom *room in selectedRooms) {
        [self.zone addRoom:room completionHandler:^(NSError *error) {
            
        }];
    }
    
    if(completion) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            completion();
        });
    }
}

#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return displayedRooms.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"RoomCell"];
    
    HMRoom *room = [displayedRooms objectAtIndex:indexPath.row];
    cell.textLabel.text = room.name;
    
    cell.accessoryType = [selectedRooms containsObject:room] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HMRoom *room = [displayedRooms objectAtIndex:indexPath.row];
    if([selectedRooms containsObject:room]) {
        [selectedRooms removeObject:room];
    } else {
        [selectedRooms addObject:room];
    }
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}



@end
