//
//  HomeKitObjectsViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitEnums.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface HomeKitObjectsViewController : UITableViewController

@property (nonatomic, assign) HomeKitObjectSection objectType;


#pragma mark - TableView datasource - Helper

-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath;
-(UITableViewCell*)addCellForRowAtIndexPath:(NSIndexPath*)indexPath;
-(NSString*)reuseIdentifierForCellForHomeKitObject:(HomeKitObjectSection)objectType;

@end
