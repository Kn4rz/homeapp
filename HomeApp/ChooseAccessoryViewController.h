//
//  ChooseAccessoryViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface ChooseAccessoryViewController : UITableViewController <HMAccessoryDelegate>

@property (nonatomic, assign) bool onlyShowsControlServices;
@property (nonatomic, assign) bool isCondition;

@end