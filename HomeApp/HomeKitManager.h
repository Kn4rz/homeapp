//
//  HomeKitManager.h
//  HomeKit_KnarzSample
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <HomeKit/HomeKit.h>

@interface HomeKitManager : NSObject {
    HMHomeManager *homeManager;
    HMAccessoryBrowser *accessoryBrowser;
}

+ (HomeKitManager*)sharedManager;
@property (nonatomic, strong) HMHome *currentHome;
@property (nonatomic, strong) HMHome *primaryHome;


-(HMHomeManager*)getManager;
-(void)setManagerDelegate:(id<HMHomeManagerDelegate>)delegate;
-(void)setAccesoryBrowserDelegate:(id<HMAccessoryBrowserDelegate>)delegate;


#pragma mark - Accessory Browser

-(NSArray*)getDiscoverdAccessories;


#pragma mark - Home

-(NSArray*)getHomes;
-(HMHome*)getPrimaryHome;
-(void)updatePrimaryHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

-(void)addHome:(NSString*)homeName completion:(void (^)(HMHome *home, NSError *error))handler;
-(void)removeHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

-(void)renameHome:(NSString*)newHomeName toHome:(HMHome*)home completion:(void (^)(NSError *error))handler;


#pragma mark - Accessory

- (void)startSearchingForNewAccessories;
- (void)stopSearchingForNewAccessories;

-(void)addAccessory:(HMAccessory*)accessory toHome:(HMHome*)home completion:(void (^)(NSError *error))handler;
-(void)removeAccessory:(HMAccessory*)accessory fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

- (void)renameAccessory:(NSString *)newAccessoryName inAccessory:(HMAccessory*)accessory completion:(void (^)(NSError * error))handler;

- (void)assignAccessory:(HMAccessory *)accessory toRoom:(HMRoom *)room inHome:(HMHome*)home completion:(void (^)(NSError *error))handler;


#pragma mark - Room

-(HMRoom*)getRoomForEntireHome;

- (void)addRoom:(NSString *)roomName toHome:(HMHome*)home completion:(void (^)(HMRoom *room, NSError *error))handler;
- (void)removeRoom:(HMRoom *)room fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

- (void)renameRoom:(NSString *)newRoomName inRoom:(HMRoom*)room completion:(void (^)(NSError * error))handler;


#pragma mark - Zone

- (void)addZone:(NSString *)zoneName toHome:(HMHome*)home completion:(void (^)(HMZone *zone, NSError *error))handler;
- (void)removeZone:(HMZone *)zone fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

- (void)addRoomToZone:(HMRoom *)room toZone:(HMZone*)zone completion:(void (^)(NSError * error))handler;
- (void)removeRoomFromZone:(HMRoom *)room fromZone:(HMZone*)zone completion:(void (^)(NSError * error))handler;

- (void)renameZone:(NSString *)newZoneName inZone:(HMZone*)zone completion:(void (^)(NSError * error))handler;


#pragma mark - ActionSet (Scene)

- (void)addActionSet:(NSString *)actionSetName toHome:(HMHome*)home completion:(void (^)(HMActionSet *actionSet, NSError *error))handler;
- (void)removeActionSet:(HMActionSet *)actionSet fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

- (void)renameActionSet:(NSString *)newActionSetName inActionSet:(HMActionSet*)actionSet completion:(void (^)(NSError * error))handler;


#pragma mark - Trigger

- (void)addTrigger:(HMTrigger *)trigger toHome:(HMHome*)home completion:(void (^)(NSError *error))handler;
- (void)removeTrigger:(HMTrigger *)trigger fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler;

- (void)renameTrigger:(NSString *)newTriggerName inTrigger:(HMTrigger*)trigger completion:(void (^)(NSError * error))handler;

@end

