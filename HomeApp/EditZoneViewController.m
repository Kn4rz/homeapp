//
//  EditZoneViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditZoneViewController.h"
#import "AddRoomViewController.h"
#import "TableViewNameCell.h"
#import "HomeKitManager.h"

@interface EditZoneViewController () {
    UIBarButtonItem *saveButton;
    UITextField *_nameField;
    
    NSMutableArray *rooms;
}
@end

@implementation EditZoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.title = self.selectedZone.name;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NameCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NameCell"];
    
    saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    saveButton.enabled = NO;
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadData {
    rooms = [[NSMutableArray alloc] initWithArray:self.selectedZone.rooms];
    [self.tableView reloadData];
}


-(void)enableSaveButtonIfNecessary {
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    saveButton.enabled = trimmedName.length > 0 && ![self.selectedZone.name isEqualToString:trimmedName];
}

-(void)saveButtonPressed {
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    [[HomeKitManager sharedManager] renameZone:trimmedName inZone:self.selectedZone completion:^(NSError *error) {
        if (error) {
            NSLog(@"Error renaming zone: %@", error);
        } else {
            NSLog(@"Zone renamed");
            self.title = self.selectedZone.name;
            if ([_nameField isFirstResponder]) {
                [_nameField resignFirstResponder];
            }
        }
    }];
}


#pragma mark - TableViewNameCell Delegate

-(void)nameFieldDidChanged {
    [self enableSaveButtonIfNecessary];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return nil;
    }
    return @"Rooms";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    }
    return rooms.count + 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        TableViewNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
        if (cell == nil) {
            cell = [[TableViewNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
        }
        _nameField = cell.nameField;
        cell._delegate = self;
        
        cell.nameField.text = self.selectedZone.name;
        return cell;
    }
    
    if (indexPath.row == rooms.count) {
        NSString *reuseIdentifier = rooms.count < [HomeKitManager sharedManager].currentHome.rooms.count ? @"AddCell" : @"DisabledAddCell";
        return [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"RoomCell"];
    
    HMRoom *room = [rooms objectAtIndex:indexPath.row];
    cell.textLabel.text = room.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        HMRoom *roomToRemove = [rooms objectAtIndex:indexPath.row];
        [rooms removeObjectAtIndex:indexPath.row];
        [[HomeKitManager sharedManager] removeRoomFromZone:roomToRemove fromZone:self.selectedZone completion:^(NSError *error){
            
            NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
            [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        [self.tableView reloadData];
        
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Rooms"]) {
        AddRoomViewController *arvc = (AddRoomViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        arvc.zone = self.selectedZone;
    }
}

@end

