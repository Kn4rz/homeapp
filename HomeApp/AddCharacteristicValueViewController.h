//
//  AddCharacteristicValueViewController.h
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface AddCharacteristicValueViewController : UITableViewController

@property (strong, nonatomic) HMCharacteristic *selectedCharacteristic;

@property (nonatomic, assign) bool isAdding;
@property (nonatomic, assign) bool isCondition;

@end
