//
//  TableViewDatePickerCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TableViewDatePickerCellDelegate <NSObject>
-(void) datePickerDidChanged;
@end


@interface TableViewDatePickerCell : UITableViewCell {
    __weak id myDelegate;
}

+ (UITableView *)styleTableView:(UITableView *)_tableView forId:(NSString *)cellId;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;


@property (nonatomic, weak) id<TableViewDatePickerCellDelegate> _delegate;


@end
