//
//  EditSceneViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitEnums.h"
#import "TableViewNameCell.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditSceneViewController : UITableViewController <HMAccessoryDelegate, TableViewNameCellDelegate>

@property (nonatomic, strong) HMActionSet *actionSet;
@property (nonatomic, assign) bool isAdding;

@end
