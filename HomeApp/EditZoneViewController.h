//
//  EditZoneViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TableViewNameCell.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditZoneViewController : UITableViewController <TableViewNameCellDelegate>

@property (strong, nonatomic) HMZone *selectedZone;

@end