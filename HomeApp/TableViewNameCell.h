//
//  TableViewNameCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TableViewNameCellDelegate <NSObject>
-(void) nameFieldDidChanged;
@end


@interface TableViewNameCell : UITableViewCell <UITextFieldDelegate> {
    __weak id myDelegate;
}

+ (UITableView *)styleTableView:(UITableView *)_tableView forId:(NSString *)cellId;
@property (weak, nonatomic) IBOutlet UITextField *nameField;

@property (nonatomic, weak) id<TableViewNameCellDelegate> _delegate;


@end
