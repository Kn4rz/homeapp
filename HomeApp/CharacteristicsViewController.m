//
//  CharacteristicsViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "AppDelegate.h"
#import "CharacteristicsViewController.h"
#import "CharacteristicCell.h"
#import "SwitchCharacteristicCell.h"
#import "SliderCharacteristicCell.h"
#import "SegmentedControlCharacteristicCell.h"
#import "TextCharacteristicCell.h"
#import "CharacteristicStringFormatter.h"


@interface CharacteristicsViewController (){
    
    NSArray *numericFormats;
    
    
    NSMutableArray *serviceCharacterstics;
    
}

@end


@implementation CharacteristicsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CharacteristicCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CharacteristicCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SwitchCharacteristicCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SwitchCharacteristicCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SliderCharacteristicCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SliderCharacteristicCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SegmentedControlCharacteristicCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SegmentedControlCharacteristicCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TextCharacteristicCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TextCharacteristicCell"];
    
    
    self.title = self.selectedService.name;
    
    numericFormats = [[NSMutableArray alloc] initWithObjects:HMCharacteristicMetadataFormatInt,
                                                              HMCharacteristicMetadataFormatFloat,
                                                              HMCharacteristicMetadataFormatUInt8,
                                                              HMCharacteristicMetadataFormatUInt16,
                                                              HMCharacteristicMetadataFormatUInt32,
                                                              HMCharacteristicMetadataFormatUInt64, nil];
    
    
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCharacteristicValue:) name:@"characteristicValueChanged" object:nil];
    
    for (HMCharacteristic* charateristic in serviceCharacterstics) {
        if ([charateristic.properties containsObject:HMCharacteristicPropertySupportsEventNotification]) {
            [charateristic enableNotification:YES completionHandler:^(NSError *error){
                
                if (error) {
                    NSLog(@"%@", error.description);
                }
                
            }];
        }
    }
    
    //    serviceCharacterstics = [[NSMutableArray alloc] init];
    //
    //    if (self.selectedService) {
    //        [self updateTableViewData];
    //    }
}


#pragma mark - Loading Data

-(void)loadData{
    self.selectedService.accessory.delegate = self;
    
    serviceCharacterstics = [[NSMutableArray alloc] initWithArray:self.selectedService.characteristics];
    [self.tableView reloadData];
}


#pragma mark - Button Actions

- (IBAction)renameService:(id)sender {
    
    //   __weak typeof (self) weakSelf = self;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Rename Service" message:@"Type new name for this service." preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:nil];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
        
        [self.selectedService updateName:textField.text completionHandler:^(NSError *error){
            
            self.title = self.selectedService.name;
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - Characteristic update methods

-(void)updateCharacteristicValue:(NSNotification*)notification {
    
    HMCharacteristic *characteristic = [[notification userInfo] objectForKey:@"characteristic"];
    
    if ([serviceCharacterstics containsObject:characteristic]) {
        NSInteger index = [serviceCharacterstics indexOfObject:characteristic];
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            cell.textLabel.text = [NSString stringWithFormat:@"%@",characteristic.value];
        });
    }
}


-(void)changeSliderValue:(id)sender {
    
    UISlider *slider = (UISlider*) sender;
    
    NSLog(@"%f", slider.value);
    
    CGPoint sliderOriginInTableView = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:sliderOriginInTableView];
    
    HMCharacteristic *characteristic = [serviceCharacterstics objectAtIndex:indexPath.row];
    
    [characteristic writeValue:[NSNumber numberWithFloat:slider.value] completionHandler:^(NSError *error){
        
        if(error == nil){
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.tableView cellForRowAtIndexPath:indexPath].textLabel.text = [NSString stringWithFormat:@"%.0f", slider.value] ;
            });
        } else {
            NSLog(@"error in writing characterstic: %@",error);
        }
    }];
    
}

-(void)changeCharacteristic:(HMCharacteristic*)characteristic atIndexPath:(NSIndexPath*)indexPath withValue:(NSNumber*)value {
    
    [characteristic writeValue:value completionHandler:^(NSError *error){
        
        if(error == nil){
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.tableView cellForRowAtIndexPath:indexPath].textLabel.text = [NSString stringWithFormat:@"%@", value] ;
            });
        } else {
            NSLog(@"error in writing characterstic: %@",error);
        }
        
    }];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateServices:(HMAccessory *)accessory {
    [self loadData];
}

- (void)accessory:(HMAccessory *)accessory service:(HMService *)service didUpdateValueForCharacteristic:(HMCharacteristic *)characteristic {
    [self loadData];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return serviceCharacterstics.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Characteristics:
            return [self characteristicCellForRowAtIndexPath:indexPath];
            
        default:
            break;
    }
   
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - TableView datasource Helper

-(CharacteristicTableViewSection)sectionForIndex:(NSInteger)section {
    switch (section) {
        case 0:
            return Characteristics;
            
        case 1:
            return AssociatedServiceType;
            
        default:
            return UnknownCharacteristicSection;
    }
}

-(UITableViewCell*)characteristicCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    HMCharacteristic *characteristic = [serviceCharacterstics objectAtIndex:indexPath.row];
    
    NSString *reuseIdentifier = @"CharacteristicCell";
    
    if((![characteristic.properties containsObject:HMCharacteristicPropertyWritable] && [characteristic.properties containsObject:HMCharacteristicPropertyReadable]) ||
       (![characteristic.properties containsObject:HMCharacteristicPropertyReadable] && [characteristic.properties containsObject:HMCharacteristicPropertyWritable])) {
        
        reuseIdentifier = @"CharacteristicCell";
        
    } else if([characteristic.metadata.format isEqualToString:HMCharacteristicMetadataFormatBool]) {
        reuseIdentifier = @"SwitchCharacteristicCell";
        
    } else if ([CharacteristicStringFormatter predeterminedValueDescriptionForNumber:characteristic] != nil) {
        reuseIdentifier = @"SegmentedControlCharacteristicCell";
    
    } else if([numericFormats containsObject:characteristic.metadata.format]) {
        reuseIdentifier = @"SliderCharacteristicCell";
    
    } else if ([characteristic.metadata.format isEqualToString:HMCharacteristicMetadataFormatString] &&
               [characteristic.properties containsObject:HMCharacteristicPropertyWritable]) {
        
        reuseIdentifier = @"TextCharacteristicCell";
    }
    
    CharacteristicCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    [cell setCharacteristicValue:characteristic];
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

@end

