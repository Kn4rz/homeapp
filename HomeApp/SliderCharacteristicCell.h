//
//  SliderCharacteristicCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "CharacteristicCell.h"
#import <UIKit/UIKit.h>

@interface SliderCharacteristicCell : CharacteristicCell

@property (weak, nonatomic) IBOutlet UISlider *valueSlider;

@end
