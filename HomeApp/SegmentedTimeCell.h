//
//  SegmentedTimeCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentedTimeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;


@end
