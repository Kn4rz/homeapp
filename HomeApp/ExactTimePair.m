//
//  ExactTimePair.m
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "ExactTimePair.h"
#import "TimeConditionViewController.h"

@implementation ExactTimePair


-(id)init:(TimeConditionOrder)timeOrder withDateComponents:(NSDateComponents*)dateComponents {
    
    self.timeOrder = timeOrder;
    self.dateComponents = dateComponents;
    
    return self;
}

@end
