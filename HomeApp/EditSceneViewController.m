//
//  EditSceneViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditSceneViewController.h"
#import "AddCharacteristicValueViewController.h"
#import "EditActionViewController.h"
#import "ChooseAccessoryViewController.h"
#import "TableViewNameCell.h"
#import "CharacteristicStringFormatter.h"
#import "HomeKitManager.h"

@interface EditSceneViewController () {
    
    UIBarButtonItem *saveButton;
    UITextField *_nameField;
    
    NSString *tmpSceneName;
    
    NSMutableArray *actions;
    NSMutableArray *displayedAccessories;
}
@end


@implementation EditSceneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if(self.actionSet != nil) {
        self.title = self.actionSet.name;
        [self nameFieldDidChanged];
    }

    
    [self.tableView registerNib:[UINib nibWithNibName:@"NameCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NameCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"CharacteristicChoosed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAction) name:@"ActionUpdated" object:nil];
    
    
    saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    saveButton.enabled = NO;
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    actions = [[NSMutableArray alloc] initWithArray:[self.actionSet.actions allObjects]];
    
    displayedAccessories = [[NSMutableArray alloc] init];
    for(HMAccessory *accessory in [[HomeKitManager sharedManager] getPrimaryHome].accessories) {
        accessory.delegate = self;
        [displayedAccessories addObject:accessory];
    }
    [self.tableView reloadData];
}

-(NSArray*)controlAccessories {
    NSMutableArray *filteredAccessories = [[NSMutableArray alloc] init];
    for (HMAccessory *accessory in [HomeKitManager sharedManager].currentHome.accessories) {
        for(HMService *service in accessory.services) {
            if(!([service.serviceType isEqualToString:HMServiceTypeAccessoryInformation] ||
                 [service.serviceType isEqualToString:HMServiceTypeLockManagement]) ){
                
                [filteredAccessories addObject:accessory];
            }
        }
    }
    return filteredAccessories;
}


#pragma mark - Notification

- (void)receiveNotification:(NSNotification *) notification {
    
    NSDictionary *userInfo = notification.userInfo;
    HMCharacteristic *choosedCharacteristic = [userInfo objectForKey:@"characteristic"];
    NSNumber *value = [userInfo objectForKey:@"characteristic-value"];

    NSLog(@"%@", choosedCharacteristic);
    [self createNewAction:choosedCharacteristic value:value];
}

- (void)updateAction {
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:1];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    if(self.isAdding) {
        saveButton.enabled = trimmedName.length > 0 && ![self.actionSet.name isEqualToString:trimmedName];
        
    } else {
        saveButton.enabled = true;

    }
}

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveButtonPressed {
    
    if(self.isAdding) {
        
        NSString *actionSetName = [_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        [[HomeKitManager sharedManager] addActionSet:actionSetName toHome:[HomeKitManager sharedManager].currentHome completion:^(HMActionSet *actionSet, NSError *error) {
            
            if(error) {
                NSLog(@"Error adding actionSet %@", error);
                
            } else {
                
                for(HMAction *action in actions) {
                    [actionSet addAction:action completionHandler:^(NSError * _Nullable error) {
                        if(error) {
                            NSLog(@"Error adding action %@", error);
                            
                        } else {
                             NSLog(@"Action added to ActionSet");
                        }
                    }];
                }
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
           
        }];
    } else {
        
        NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        if (trimmedName.length > 0 && ![self.actionSet.name isEqualToString:trimmedName])
            [self updateActionSetName:trimmedName];
        
        
        if([self.actionSet.actions count] != [actions count]) {
            
            [self removeAllActionSetsFromActionSet];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addAllSelectedActionSetsToActionSet];
            });
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - ActionSet update methods

-(void)updateActionSetName:(NSString*)name {
    [[HomeKitManager sharedManager] renameActionSet:name inActionSet:self.actionSet completion:^(NSError *error) {
        
        if(error) {
            NSLog(@"Error renaming actionSet %@", error);
            
        } else {
            NSLog(@"ActionSet renamed");
        }
    }];
}

-(void)removeAllActionSetsFromActionSet {
    for(HMAction *action in self.actionSet.actions) {
        [self.actionSet removeAction:action completionHandler:^(NSError * _Nullable error) {
            if(error) {
                NSLog(@"Error removing actionSet %@", error);
                
            } else {
                NSLog(@"ActionSet removed");
            }
        }];
    }
}

-(void)addAllSelectedActionSetsToActionSet {
    for(HMAction *action in actions) {
        [self.actionSet addAction:action completionHandler:^(NSError * _Nullable error) {
            if(error) {
                NSLog(@"Error adding action %@", error);
                
            } else {
                NSLog(@"Action added to ActionSet");
            }
        }];
    }
}


#pragma mark - Action creation

-(void)createNewAction:(HMCharacteristic*)characteristic value:(NSNumber*)value {
    
    HMCharacteristicWriteAction *newAction = [[HMCharacteristicWriteAction alloc] initWithCharacteristic:characteristic targetValue:value];
    [actions addObject:newAction];

    [self enableSaveButtonIfNecessary];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:1];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateReachability:(HMAccessory *)accessory {
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:2];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - TableViewNameCell Delegate

-(void)nameFieldDidChanged {
    tmpSceneName = (_nameField.text.length > 0 && ![_nameField.text isEqualToString:tmpSceneName]) ? _nameField.text : tmpSceneName;
    [self enableSaveButtonIfNecessary];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return nil;
            
        case 1:
            return @"Actions";
            
        case 2:
            return @"Accessories";
            
        default:
            return @"undefined";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    }
    if(section == 1) {
        return actions.count + 1;
    }
    if(section == 2) {
        return displayedAccessories.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        TableViewNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
        if (cell == nil) {
            cell = [[TableViewNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
        }
        _nameField = cell.nameField;
        cell._delegate = self;
        
        cell.nameField.text = self.isAdding ? tmpSceneName : self.actionSet.name;
        return cell;
    }
    
    if(indexPath.section == 1) {
        // TODO
//        return [self.tableView dequeueReusableCellWithIdentifier:@"NoActionsCell"];
        
        if([self cellTypeForIndexPath:indexPath] == Add) {
            return [self addCellForRowAtIndexPath:indexPath];
            
        } else {
            
            HMCharacteristicWriteAction *action = [actions objectAtIndex:indexPath.row];
            HMService *service = action.characteristic.service;
            HMAccessory *accessory = service.accessory;
            
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ActionCell"];
            cell.textLabel.text = [CharacteristicStringFormatter localizedStringFromCharacteristic:action.characteristic withValue:action.targetValue];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ in %@", service.name, accessory.name];
            return cell;
        }
    }
    
    HMAccessory *accessory = [displayedAccessories objectAtIndex:indexPath.row];
    NSString *reuseIdentifier = accessory.isReachable ? @"AccessoryCell" : @"UnreachableAccessoryCell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = accessory.name;
    
    if (accessory.isReachable) {
        cell.textLabel.textColor = UIColor.darkTextColor;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    else {
        cell.textLabel.textColor = UIColor.lightGrayColor;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        HMAction *action = [actions objectAtIndex:indexPath.row];
        
        [self.actionSet removeAction:action completionHandler:^(NSError * _Nullable error) {
            if(error) {
                NSLog(@"Error removing action %@", error);
            } else {
                NSLog(@"Action removed");
                
                [actions removeObject:action];
                [self enableSaveButtonIfNecessary];
                
                NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:1];
                [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }];
        
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        if(indexPath.section == 1) {
            
            [self performSegueWithIdentifier:@"Add New Action" sender:self];
        }
    }
}


#pragma mark - TableView datasource - Helper

-(UITableViewCell*)addCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    NSString *reuseIdentifier = @"AddCell";
    
    UITableViewCell *cell =  [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = @"Add Action…";
    return cell;
}

-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath {
    
    NSInteger objectCount = [actions count];
    if (indexPath.row == objectCount) {
        return Add;
    }
    else {
        return Object;
    }
    
}


#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"Edit Action"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        HMAction *action = [actions objectAtIndex:indexPath.row];
        if([self.actionSet.actions containsObject:action]) {
            return YES;
            
        } else {
            return NO;
        }
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Edit Action"]) {
            EditActionViewController *acvvc = (EditActionViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            acvvc.selectedAction = [actions objectAtIndex:indexPath.row];
        }
    } else {
        
        if ([segue.identifier isEqualToString:@"Add New Action"]) {
            ChooseAccessoryViewController *vc = (ChooseAccessoryViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            vc.onlyShowsControlServices = true;
        }
    }
}

@end
