//
//  SliderCharacteristicCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "SliderCharacteristicCell.h"

@implementation SliderCharacteristicCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCharacteristicValue:(HMCharacteristic*)characteristic {
    [super setCharacteristicValue:characteristic];
    
    self.valueSlider.maximumValue = [characteristic.metadata.maximumValue floatValue];
    self.valueSlider.minimumValue = [characteristic.metadata.minimumValue floatValue];
    self.valueSlider.value = [characteristic.value floatValue];
    self.valueSlider.continuous = true;
}


- (IBAction)didChangeSliderValue:(id)sender {
    
    float roundedValue = [self roundedValueForSliderValue:self.valueSlider.value];
    [self.characteristic writeValue:[NSNumber numberWithFloat:roundedValue] completionHandler:^(NSError *error){
        
        if(error == nil){
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self updateValue:self.characteristic.value];
            });
        } else {
            NSLog(@"error in writing characterstic: %@",error);
        }
    }];
    
}


-(float)roundedValueForSliderValue:(float)value {
    if(self.characteristic.metadata) {
        
        NSNumber *stepValue = self.characteristic.metadata.stepValue;
        
        if(stepValue > 0) {
            float newStep = roundf(value/[stepValue floatValue]);
            float stepped = newStep * [stepValue floatValue];
            return stepped;
        }
        
    }
    return value;
}

@end
