//
//  EditEventTriggerViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditEventTriggerViewController.h"
#import "TimeConditionViewController.h"
#import "ChooseAccessoryViewController.h"
#import "CharacteristicStringFormatter.h"
#import "SunStatePair.h"
#import "ExactTimePair.h"
#import <Foundation/NSExpression.h>

@interface EditEventTriggerViewController () {
    NSDictionary *characteristicPair;
    SunStatePair *sunStatePair;
    ExactTimePair *exactTimePair;
}
@end

@implementation EditEventTriggerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newConditionCreated:) name:@"NewConditionCreated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newActionConditionCreated:) name:@"NewActionConditionCreated" object:nil];
    
    self.eventTrigger = (HMEventTrigger*)self.trigger;
}


#pragma mark - Loading Data

-(void) loadData {
    [super loadData];
    
    self.conditions = self.isAdding ? [[NSMutableArray alloc] init] : [[NSMutableArray alloc] initWithArray:[self loadConditionsFromEventTrigger:self.eventTrigger]];
}

-(NSArray*)loadConditionsFromEventTrigger:(HMEventTrigger*)trigger {
    NSCompoundPredicate *compoundPredicate = (NSCompoundPredicate*)trigger.predicate;
    return compoundPredicate.subpredicates;
}


#pragma mark - Notification

- (void)newConditionCreated:(NSNotification *) notification {
    NSPredicate *predicate = (NSPredicate*)notification.object;
    [self.conditions addObject:predicate];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:[self sectionForConditions]];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)newActionConditionCreated:(NSNotification *) notification {
    
    NSDictionary *userInfo = (NSDictionary*)notification.userInfo;
    HMCharacteristic *characteristic = [userInfo objectForKey:@"characteristic"];
    NSNumber *value = [userInfo objectForKey:@"characteristic-value"];
    
    NSPredicate *predicate = [HMEventTrigger predicateForEvaluatingTriggerWithCharacteristic:characteristic relatedBy:NSEqualToPredicateOperatorType toValue:value];
    [self.conditions addObject:predicate];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:[self sectionForConditions]];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    
    NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    if(self.isAdding) {
        self.saveButton.enabled = (trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName]);
        
    } else {
        self.saveButton.enabled = true;
    }
}


#pragma mark - New Event Trigger creation

-(HMEventTrigger*)newTrigger {
    
    NSString *triggerName = [self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    NSArray *events = [self createEventsForEventTrigger];
    
    NSCompoundPredicate *predicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:self.conditions];
    
    return [[HMEventTrigger alloc] initWithName:triggerName events:events predicate:predicate];
}

-(NSArray*)createEventsForEventTrigger {
    return [[NSMutableArray alloc] init];
}


#pragma mark - Event Trigger update methods

-(void)updateEventTriggerConditions:(HMEventTrigger*)trigger {
    NSCompoundPredicate *predicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:self.conditions];
    [trigger updatePredicate:predicate completionHandler:^(NSError * _Nullable error) {
        if(error) {
            NSLog(@"Error updating conditions in event trigger %@", error);
            
        } else {
            NSLog(@"Conditions in event Trigger updated");
        }
    }];
}


#pragma mark - Conditon delete methods

-(void)deleteCondition:(NSIndexPath*)indexPath {
    [self.conditions removeObjectAtIndex:indexPath.row];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    
    NSCompoundPredicate *predicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:self.conditions];
    [self.eventTrigger updatePredicate:predicate completionHandler:^(NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Error updating conditions on event trigger %@", error);
            
        } else {
            NSLog(@"Conditions updated on event trigger");
        }
    }];
}


#pragma mark - Condition creation methods

-(void)showNewConditionView {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Add Condition" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Time" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Show Time Condition" sender:self];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Action" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Show Action Condition" sender:self];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}



#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
            
        case Conditions:
            return @"Conditions";
            
        default:
            return [super tableView:tableView titleForHeaderInSection:section];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
            
        case Conditions:
            return self.conditions.count + 1;
            
        default:
            return [super tableView:tableView numberOfRowsInSection:section];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Conditions:
            
            switch ([self cellTypeForIndexPath:indexPath]) {
                case Add:
                    return [self addCellForRowAtIndexPath:indexPath];
                    
                case Object:
                    return [self conditionCellForRowAtIndexPath:indexPath];
                    
                default:
                    return nil;
            }
    
        default:
            return [super tableView:tableView cellForRowAtIndexPath:indexPath];;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self sectionForIndex:indexPath.section]) {
            
        case Conditions:
            return !([self cellTypeForIndexPath:indexPath] == Add);
            
        default:
            return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        switch ([self sectionForIndex:indexPath.section]) {
            case Conditions:
                [self deleteCondition:indexPath];
                break;
                
            default:
                break;
        }
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Conditions:
            
            if([self cellTypeForIndexPath:indexPath] == Add) {
                [self showNewConditionView];
            }
            
            break;
            
        default:
            [super tableView:tableView didSelectRowAtIndexPath:indexPath];
            break;
    }
}


#pragma mark - TableView datasource Helper

-(int)sectionForConditions {
    return 0;
}

-(NSString*)titleForAddRowInSection:(NSIndexPath*)indexPath {
    switch ([self sectionForIndex:indexPath.section]) {
            
        case Conditions:
            return @"Add Condition…";
            
        default:
            return @"undefined";
    }
}

-(UITableViewCell*)addCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    NSString *reuseIdentifier = @"AddCell";
    
    UITableViewCell *cell =  [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = [self titleForAddRowInSection:indexPath];
    return cell;
}

-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath {
    
    NSInteger objectCount = [(NSArray*)[self objectsForSection:indexPath.section] count];
    if (indexPath.row == objectCount) {
        return Add;
    }
    else {
        return Object;
    }
}

-(NSObject*) objectsForSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case Conditions:
            return self.conditions;
            
        default:
            return [[NSMutableArray alloc] init];
    }
}

-(UITableViewCell*)conditionCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    NSPredicate *condition = [self.conditions objectAtIndex:indexPath.row];
    switch ([self homeKitConditionType:condition]) {
        case CharacteristicType:
            return [self characteristicTypeCell];
            
        case ExactTimeType:
            return [self exactTimeCell:exactTimePair];

        case SunTimeType:
            return [self sunStateCell:sunStatePair];
            
        default:
            return nil;
    }
}

-(UITableViewCell*)characteristicTypeCell {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ConditionCell"];
    
    HMCharacteristic *characteristic = [characteristicPair objectForKey:@"characteristic"];
    id value = [characteristicPair objectForKey:@"characteristic-value"];
    
    HMService *service = characteristic.service;
    HMAccessory *accessory = service.accessory;
    
    cell.textLabel.text = [CharacteristicStringFormatter localizedStringFromCharacteristic:characteristic withValue:value];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ in %@", service.name, accessory.name];
    
    return cell;
}

-(UITableViewCell*)exactTimeCell:(ExactTimePair*)exactTimePair {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ConditionCell"];
    
    switch (exactTimePair.timeOrder) {
        case Before:
            cell.textLabel.text = [NSString stringWithFormat:@"Before %@", [self conditionCellExactTimeString:exactTimePair]];
            cell.detailTextLabel.text = @"Relative to Time";
            break;
            
        case After:
            cell.textLabel.text = [NSString stringWithFormat:@"After %@", [self conditionCellExactTimeString:exactTimePair]];
            cell.detailTextLabel.text = @"Relative to Time";
            break;
            
        case At:
            cell.textLabel.text = [NSString stringWithFormat:@"At %@", [self conditionCellExactTimeString:exactTimePair]];
            cell.detailTextLabel.text = @"Relative to Time";
            break;
    }
    
    return cell;
}

-(UITableViewCell*)sunStateCell:(SunStatePair*)sunStatePair {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ConditionCell"];
    
    switch (sunStatePair.timeOrder) {
        case Before:
            cell.textLabel.text = [NSString stringWithFormat:@"Before %@", [self conditionCellSpecialEventString:sunStatePair]];
            cell.detailTextLabel.text = @"Relative to sun";
            break;
            
        case After:
            cell.textLabel.text = [NSString stringWithFormat:@"After %@", [self conditionCellSpecialEventString:sunStatePair]];
            cell.detailTextLabel.text = @"Relative to sun";
            break;
    }
    
    return cell;
}



-(NSString*)conditionCellExactTimeString:(ExactTimePair*)exactTimePair {
    
    NSDateComponents *comps = exactTimePair.dateComponents;
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *timeString = [formatter stringFromDate:date];
    
    return timeString;
}

-(NSString*)conditionCellSpecialEventString:(SunStatePair*)sunTimePair {
    NSString *timeString;
    switch (sunTimePair.sunState) {
        case Sunrise:
            timeString = @"Sunrise";
            break;
            
        case Sunset:
            timeString = @"Sunset";
            break;
    }
    
    return timeString;
}

-(HomeKitConditionType)homeKitConditionType:(NSPredicate*)predicate {

    if([self characteristicPair:predicate]) {
        characteristicPair = [self characteristicPair:predicate];
        return CharacteristicType;
    }
    
    if([self sunStatePair:predicate]) {
        sunStatePair = [self sunStatePair:predicate];
        return SunTimeType;
    }
    
    if([self exactTimePair:predicate]) {
        exactTimePair = [self exactTimePair:predicate];
        return ExactTimeType;
    }
    
    return UnknownType;
}

-(NSDictionary*)characteristicPair:(NSPredicate*)predicate {
    if([predicate isKindOfClass:[NSCompoundPredicate class]]) {
        
        NSCompoundPredicate *compoundPredicate = (NSCompoundPredicate*)predicate;
        if(compoundPredicate.subpredicates.count != 2) return nil;
        
        NSComparisonPredicate *characteristicPredicate;
        NSComparisonPredicate *valuePredicate;
        
        for (NSPredicate *subpredicate in compoundPredicate.subpredicates) {
            
            NSComparisonPredicate *comparison = (NSComparisonPredicate*)subpredicate;
            if(comparison.leftExpression.expressionType == NSKeyPathExpressionType &&
               comparison.rightExpression.expressionType == NSConstantValueExpressionType) {
                
                
                if([comparison.leftExpression.keyPath isEqualToString:HMCharacteristicKeyPath]) {
                    characteristicPredicate = comparison;
                }
                
                if([comparison.leftExpression.keyPath isEqualToString:HMCharacteristicValueKeyPath]) {
                    valuePredicate = comparison;
                }
                
            }
            
        }
        
        if(characteristicPredicate.rightExpression.constantValue) {
            HMCharacteristic *characteristic = (HMCharacteristic*)characteristicPredicate.rightExpression.constantValue;
            id characteristicValue = valuePredicate.rightExpression.constantValue;
            
            return [NSDictionary dictionaryWithObjects:@[characteristic, characteristicValue] forKeys:@[@"characteristic", @"characteristic-value"]];
        }
    }
    return nil;
}

-(SunStatePair*)sunStatePair:(NSPredicate*)predicate {
    
    if([predicate isKindOfClass:[NSComparisonPredicate class]]) {
        
        NSComparisonPredicate *comparison = (NSComparisonPredicate*)predicate;
        
        if(comparison.leftExpression.expressionType != NSKeyPathExpressionType) return nil;
        if(comparison.rightExpression.expressionType != NSFunctionExpressionType) return nil;
        if(![comparison.rightExpression.function isEqualToString:@"now"]) return nil;
        if(comparison.rightExpression.arguments.count != 0) return nil;
        
        
        if(([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunrise] &&
            comparison.predicateOperatorType == NSLessThanPredicateOperatorType) ||
           ([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunrise] &&
            comparison.predicateOperatorType == NSLessThanOrEqualToPredicateOperatorType)) {
               
               return [[SunStatePair alloc] init:After withSunState:Sunrise];
           }
        
        if(([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunrise] &&
            comparison.predicateOperatorType == NSGreaterThanPredicateOperatorType) ||
           ([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunrise] &&
            comparison.predicateOperatorType == NSGreaterThanOrEqualToPredicateOperatorType)) {
               
               return [[SunStatePair alloc] init:Before withSunState:Sunrise];
           }
        
        if(([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunset] &&
            comparison.predicateOperatorType == NSLessThanPredicateOperatorType) ||
           ([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunset] &&
            comparison.predicateOperatorType == NSLessThanOrEqualToPredicateOperatorType)) {
               
               return [[SunStatePair alloc] init:After withSunState:Sunset];
           }
        
        if(([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunset] &&
            comparison.predicateOperatorType == NSGreaterThanPredicateOperatorType) ||
           ([comparison.leftExpression.keyPath isEqualToString:HMSignificantEventSunset] &&
            comparison.predicateOperatorType == NSGreaterThanOrEqualToPredicateOperatorType)) {
               
               return [[SunStatePair alloc] init:Before withSunState:Sunset];
           }
        
    }
    return nil;
}

-(ExactTimePair*)exactTimePair:(NSPredicate*)predicate {
    
    if([predicate isKindOfClass:[NSComparisonPredicate class]]) {
        
        NSComparisonPredicate *comparison = (NSComparisonPredicate*)predicate;
        
        if(comparison.leftExpression.expressionType != NSFunctionExpressionType) return nil;
        if(![comparison.leftExpression.function isEqualToString:@"now"]) return nil;
        if(comparison.rightExpression.expressionType != NSConstantValueExpressionType) return nil;
        
        NSDateComponents *dateComponents = (NSDateComponents*)comparison.rightExpression.constantValue;
        if(dateComponents == nil) return nil;
        
        switch (comparison.predicateOperatorType) {
            case NSLessThanPredicateOperatorType:
            case NSLessThanOrEqualToPredicateOperatorType:
                
                return [[ExactTimePair alloc] init:Before withDateComponents:dateComponents];
                
            case NSGreaterThanPredicateOperatorType:
            case NSGreaterThanOrEqualToPredicateOperatorType:
                
                return [[ExactTimePair alloc] init:After withDateComponents:dateComponents];
                
            case NSEqualToPredicateOperatorType:
                
                return [[ExactTimePair alloc] init:At withDateComponents:dateComponents];
                
            default:
                return nil;
        }
    }
    return nil;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Action Condition"]) {
        ChooseAccessoryViewController *vc = (ChooseAccessoryViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        vc.isCondition = true;
        vc.onlyShowsControlServices = true;
    }
}

@end
