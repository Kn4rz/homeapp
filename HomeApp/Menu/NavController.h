//
//  NavControllerViewController.h
//  Apfelpage
//
//  Created by Tim Trinkies on 06.11.12.
//  Copyright (c) 2012 Tim Trinkies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavController : UINavigationController {
    UIButton *searchButton;
    
    bool addedBG;
}

@property(nonatomic, retain) NSNumber *dontAddNavBG;

- (void)editButtonClicked:(id)sender;
- (void)addBtn;
- (void)backButtonClicked:(id)sender;

@end
