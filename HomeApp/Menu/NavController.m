//
//  NavController.m
//  Apfelpage
//
//  Created by Tim Trinkies on 06.11.12.
//  Copyright (c) 2012 Tim Trinkies. All rights reserved.
//

#import "NavController.h"
#import "UIViewController+MMDrawerController.h"

@interface NavController ()

@end

@implementation NavController
@synthesize dontAddNavBG;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationBar.barTintColor = UIColorFromRGB(0x4d9ccf);
    self.view.backgroundColor = UIColorFromRGB(0x4d9ccf);
    
    NSDictionary *titleAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     [UIFont fontWithName:@"HelveticaNeue-Light" size:20], NSFontAttributeName, nil];
    self.navigationBar.titleTextAttributes = titleAttributes;
    self.navigationBar.translucent = NO;


    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && !addedBG){
        addedBG = TRUE;
        //[self.navigationBar addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"design/navbar/background-ht.png"]]];

    }

    [self addBtn];
}

- (void)viewDidAppear:(BOOL)animated {
//    if([self.viewControllers count]>0){
//        [self addBtn];
//    }
    
    [super viewDidAppear:animated];
    
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {

    [super pushViewController:viewController animated:animated];

    if(viewController != self.navigationController.topViewController){
        //if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad){
            UIButton *backButton = [[UIButton alloc] init];
            [backButton setImage:[UIImage imageNamed:@"n_Back.png"] forState:UIControlStateNormal];
            [backButton setImage:[UIImage imageNamed:@"n_Back.png"] forState:UIControlStateHighlighted];
            [backButton setImage:[UIImage imageNamed:@"n_Back.png"] forState:UIControlStateSelected];
            [backButton addTarget:self action:@selector(backButtonClicked:)
                 forControlEvents:UIControlEventTouchUpInside];
            
        
            backButton.frame = CGRectMake(0, 0, 28, 22);
            backButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            
            UIBarButtonItem *button = [[UIBarButtonItem alloc]
                                       initWithCustomView:backButton];
            viewController.navigationItem.leftBarButtonItem = button;
        

        }
    //}
}

- (void)backButtonClicked:(id)sender {

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        [self popViewControllerAnimated:NO];
    else
        [self popViewControllerAnimated:YES];
}

- (void)addBtn {
    searchButton = [[UIButton alloc] init];
    [searchButton setImage:[UIImage imageNamed:@"n_Sidemenu.png"] forState:UIControlStateNormal];
    [searchButton setImage:[UIImage imageNamed:@"n_Sidemenu_sel.png"] forState:UIControlStateHighlighted];
    [searchButton setImage:[UIImage imageNamed:@"n_Sidemenu_sel.png"] forState:UIControlStateSelected];
    [searchButton addTarget:self action:@selector(editButtonClicked:)
           forControlEvents:UIControlEventTouchUpInside];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        searchButton.frame = CGRectMake(0, 0, 28, 20);
        searchButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }

    UIBarButtonItem *button = [[UIBarButtonItem alloc]
                               initWithCustomView:searchButton];
    
    if([self.viewControllers count]>0){
        UIViewController *vc = [self.viewControllers objectAtIndex:0];
        vc.navigationItem.leftBarButtonItem = button;
    }
}

- (void)editButtonClicked:(id)sender {
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
            return UIInterfaceOrientationMaskPortrait;
        
    }
    return UIInterfaceOrientationMaskPortrait;
}


@end
