//
//  DrawerController.h
//  Travelcloud
//
//  Created by Tim Trinkies on 03.06.13.
//  Copyright (c) 2013 Tim Trinkies. All rights reserved.
//

#import "MMDrawerController.h"

@interface DrawerController : MMDrawerController {
    
    IBOutlet UIViewController *centerView;
    IBOutlet UIViewController *leftView;

    bool wasSVWEB;
}

@end
