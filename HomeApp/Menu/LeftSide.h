#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import <MessageUI/MessageUI.h>
#import "HomesViewController.h"
#import "NavController.h"


enum {
    SectionPicture,
    SectionsViews,
    SectionsWeiteres
};

typedef NSInteger SectionState;

@interface LeftSide : UIViewController <UITableViewDataSource,UITableViewDelegate, MFMailComposeViewControllerDelegate> {

    NSIndexPath *_selectIndexPath;
    UIViewController *_currentVC;
    NavController *_roomesVC;
    NavController *_zonesVC;
    NavController *_scenesVC;
    NavController *_triggerVC;
    NavController *_moreVC;
    
}


@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * drawerWidths;
@property (nonatomic, strong) NavController *_homesVC;

@end
