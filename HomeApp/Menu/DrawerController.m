//
//  DrawerController.m
//  Travelcloud
//
//  Created by Tim Trinkies on 03.06.13.
//  Copyright (c) 2013 Tim Trinkies. All rights reserved.
//

#import "DrawerController.h"
#import "NavController.h"

//#import "PhotoBrowserViewController.h"
//#import "PMChoosePostcardTypeViewController.h"

@interface DrawerController ()

@end

@implementation DrawerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
   /* MMDrawerController * drawerController = [[MMDrawerController alloc]
                                             initWithCenterViewController:centerView
                                             leftDrawerViewController:leftView
                                             rightDrawerViewController:nil];
    */
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)shouldAutorotate
{
    return YES;
}

//-(NSUInteger)supportedInterfaceOrientations
//{
//    if([self.centerViewController isKindOfClass:[NavController class]]){
//        NavController *nav = (NavController *)self.centerViewController;
//        
//        if([[nav topViewController] isKindOfClass:[SVWebViewController class]] || [[nav topViewController] isKindOfClass:[PhotoBrowserViewController class]])
//        {
//            //NSLog(@"LOLLLLL11111111 mask all %@", [[nav topViewController] description]);
//            wasSVWEB = TRUE;
//           return UIInterfaceOrientationMaskAll;
//        }
//    }
//    if(wasSVWEB){
//        wasSVWEB = FALSE;
//        UIViewController *viewController = [[UIViewController alloc] init];
//        [self presentViewController:viewController animated:NO completion:^{
//            [viewController dismissViewControllerAnimated:NO completion:nil];
//        }];
//    }
//    
//    //NSLog(@"LOLLLLL1111... mask portrait");
//
//    return UIInterfaceOrientationMaskPortrait;
//}


@end
