#import "LeftSide.h"
#import "MMSideDrawerTableViewCell.h"
#import "MMSideDrawerSectionHeaderView.h"
#import <MMDrawerVisualState.h>
#import <SDWebImage/UIImageView+WebCache.h>


@implementation LeftSide

@synthesize _homesVC;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"menuBackgroundNew.png"]]];
    

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y+25, self.view.bounds.size.width, self.view.bounds.size.height-25) style:UITableViewStylePlain];
    
    
    _homesVC = (NavController *)self.mm_drawerController.centerViewController;
    if ([[_homesVC.viewControllers firstObject] isKindOfClass:[HomesViewController class]]) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _homesVC = [storyboard instantiateViewControllerWithIdentifier:@"homes"];
        
        [self.mm_drawerController
         setCenterViewController:_homesVC
         withCloseAnimation:NO
         completion:nil];
    }
    
    if ([[_homesVC.viewControllers firstObject] isKindOfClass:[HomesViewController class]]) {

    }

    
    //2PAlex
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstStartDone"]) {

        
        [self performSelector:@selector(showIntro) withObject:nil afterDelay:1];
        
    }

    
    _tableView.scrollsToTop = NO;
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.view addSubview:self.tableView];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
//    [self.tableView setSeparatorColor:UIColorFromRGB(0x708381)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
//    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
//    [self.tableView setTableFooterView:footerView];
    
    self.drawerWidths = @[@(240)];
    [self.mm_drawerController setOpenDrawerGestureModeMask:(MMOpenDrawerGestureModePanningNavigationBar|MMOpenDrawerGestureModeBezelPanningCenterView)];
    [self.mm_drawerController setDrawerVisualStateBlock:[MMDrawerVisualState slideVisualStateBlock]];
    [self.mm_drawerController setMaximumLeftDrawerWidth:271];
    [self.mm_drawerController setShowsShadow:NO];

    _selectIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
}

- (void)openHomesVcRoot {
    
    [_homesVC popToRootViewControllerAnimated:NO];
    [self.mm_drawerController
     setCenterViewController:_homesVC
     withCloseAnimation:YES
     completion:nil];
}


- (void)showIntro {
//    IntroViewController *intro = [[IntroViewController alloc] init];
//    intro.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    intro.modalPresentationStyle = UIModalPresentationCustom;
//    [[_cardVC.viewControllers firstObject] presentViewController:intro animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.mm_drawerController setOpenDrawerGestureModeMask:(MMOpenDrawerGestureModePanningNavigationBar|MMOpenDrawerGestureModeBezelPanningCenterView)];
    [self.mm_drawerController setShowsShadow:NO];
    
    
    [self refreshAccountInfo];
//    [self.tableView setSeparatorColor:UIColorFromRGB(0x708381)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView	{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case SectionPicture:
            return 1;
        case SectionsViews:
            
            return 6;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *CellIdentifier2 = @"Cell2";
    UIImageView *profileImageView;
    UIImage *profileIMG;
    UILabel *credits;
    UILabel *profileName;
    
    
    if (indexPath.section == SectionPicture){

            UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                
                cell = [[MMSideDrawerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
               
                //Elements
                profileName = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, 270, 22)];
                profileName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
                profileName.textColor = UIColorFromRGB(0xdcf3ff);
                profileName.backgroundColor = [UIColor clearColor];
                profileName.tag = 100;
                profileName.textAlignment = NSTextAlignmentCenter;
                [cell.contentView addSubview:profileName];
                
                credits = [[UILabel alloc] initWithFrame:CGRectMake(0, 142, 270, 22)];
                credits.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
                credits.textColor = UIColorFromRGB(0xdcf3ff);
                credits.backgroundColor = [UIColor clearColor];
                credits.tag = 101;
                credits.textAlignment = NSTextAlignmentCenter;
                [cell.contentView addSubview:credits];
                
                profileImageView = [[UIImageView alloc] init];
                profileImageView.frame = CGRectMake(85, 10, 100, 100);
                profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
                profileImageView.layer.masksToBounds = YES;
                profileImageView.contentMode = UIViewContentModeScaleAspectFill;
                profileImageView.layer.borderColor = UIColorFromRGB(0x51bbf5).CGColor;
                profileImageView.layer.borderWidth = 0.0;
                profileImageView.tag = 102;
                [cell.contentView addSubview:profileImageView];

            
            }
            
            if (!profileName)
                profileName = (UILabel*)[cell viewWithTag:100];
            if (!credits)
                credits = (UILabel*)[cell viewWithTag:101];
            if (!profileImageView)
                profileImageView = (UIImageView*)[cell viewWithTag:102];

        

                if (indexPath.row == 0) {
//                    NSDictionary *user = [PMOauth profileInformation];
                    
//                    if (user != nil && [PMOauth isLoggedInWithUser]) {
//                        profileImageView.hidden =NO;
//                        profileName.hidden = NO;
//                        credits.hidden = NO;
//                        profileImageView.layer.borderWidth = 1.0;
//                        profileName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
//                        
//                        profileIMG = [UIImage imageNamed:@"profile.jpg"];
//                        [profileImageView sd_setImageWithURL:[NSURL URLWithString:[user objectForKey:@"avatar_url"]]];
//                        profileName.text = [user objectForKey:@"name"];
//                        
//                        
//                        if ([[user objectForKey:@"credits"] intValue] != 1) {
//                            credits.text = [[[user objectForKey:@"credits"] stringValue] stringByAppendingFormat:@" %@", NSLocalizedString(@"LABEL_CreditStatus_Cards", "")];
//                        }else{
//                            credits.text = [[[user objectForKey:@"credits"] stringValue] stringByAppendingFormat:@" %@", NSLocalizedString(@"LABEL_CreditStatus_Card", "")];
//                        }
//                        
//        
//                        
//                    }
//                    else {
//                        profileImageView.image = [UIImage imageNamed:@"profile-login@2x.png"];
//                        profileImageView.layer.borderWidth = 0.0;
//                        profileName.text = @"Login";
//                        profileName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
//                        credits.hidden = YES;
                    }


//                }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == SectionsViews){
            UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
            UIView *singleLine;
            if (cell == nil) {
            
                cell = [[MMSideDrawerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
                
                
                
                int cellHeight = 60;
                
                if (IS_IPHONE_4) {
                    cellHeight = 40;
                }
                else if (IS_IPHONE_5) {
                    cellHeight = 50;
                }
                
                singleLine = [[UILabel alloc] initWithFrame:CGRectMake(50, cellHeight-1, self.tableView.bounds.size.width-20, 1)];
                singleLine.backgroundColor = [UIColor whiteColor];
                singleLine.tag = 104;
                singleLine.alpha = .8;
                [cell.contentView addSubview:singleLine];
            }
        
            int Path = -1;

        
        
            if(indexPath.row == Path){
                
                [cell.textLabel setText:NSLocalizedString(@"Current Card", @"Menu_CurrentCard")];
            }
            else if(indexPath.row == 1+Path){
              
                [cell.textLabel setText:NSLocalizedString(@"My Home", @"Menu_Homes")];
            }
            else if(indexPath.row == 2+Path){
                [cell.textLabel setText:NSLocalizedString(@"My Rooms", @"Menu_Rooms")];
            }
            else if(indexPath.row == 3+Path){
                [cell.textLabel setText:NSLocalizedString(@"My Zones", @"Menu_Zones")];
            }
            else if(indexPath.row == 4+Path){
                [cell.textLabel setText:NSLocalizedString(@"My Scenes", @"Menu_Scenes")];
            }
            else if(indexPath.row == 5+Path){
                [cell.textLabel setText:NSLocalizedString(@"My Trigger", @"Menu_Trigger")];
            }
            else if(indexPath.row == 6+Path){
                [cell.textLabel setText:NSLocalizedString(@"More", @"Menu_More")];
            }
        
            if(_selectIndexPath.row == indexPath.row && _selectIndexPath.section == _selectIndexPath.section){
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:19];
                cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i_%i-sel.png", (int)indexPath.section, (int)indexPath.row-Path]];
                
            }else {
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:19];
                cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i_%i.png", (int)indexPath.section, (int)indexPath.row-Path]];
                
            }
        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }

    return nil;

    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case SectionPicture:
            return @"";
        case SectionsViews:
            return @"";
        default:
            return @"";
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    MMSideDrawerSectionHeaderView * headerView =  [[MMSideDrawerSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 30.0f)];
    
   
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [headerView setTitle:[tableView.dataSource tableView:tableView titleForHeaderInSection:section]];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == SectionPicture) {
        
        if (IS_IPHONE_4) {
            return 164;
        }
        else if (IS_IPHONE_5) {
//            return ([PMCardManager currentCard].isAnyImageSet || [PMCardManager currentCard].greetingsText.length>0) ? 160 : 170;
            return 164;
        }
            return 164;
//        return ([PMCardManager currentCard].isAnyImageSet || [PMCardManager currentCard].greetingsText.length>0) ? 182 : 196;
    }
    
    
    if (IS_IPHONE_4) {
        return 40;
    }
    else if (IS_IPHONE_5) {
        return 47;
    }
    return 54.0;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SectionsViews) {
        
        
        int Path = -1;
//        if ([PMCardManager currentCard].isAnyImageSet || [PMCardManager currentCard].greetingsText.length>0){
//            Path = 0;
//        }
        [tableView cellForRowAtIndexPath:indexPath].textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:19];
        [tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i_%i-sel.png", (int)indexPath.section, (int)indexPath.row-Path]];
    }
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SectionsViews) {
        
        
        int Path = -1;
//        if ([PMCardManager currentCard].isAnyImageSet || [PMCardManager currentCard].greetingsText.length>0){
//            Path = 0;
//        }
        [tableView cellForRowAtIndexPath:indexPath].textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:19];
        [tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i_%i.png", (int)indexPath.section, (int)indexPath.row-Path]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (_selectIndexPath == [NSIndexPath indexPathForRow:0 inSection:1] && (indexPath != [NSIndexPath indexPathForRow:0 inSection:1])) {
//        if ([PMCardManager currentCard].needsCardArchiving) {
//            [PMCardManager archiveCurrentCard];
//        }
//        
//    }
    
    switch (indexPath.section) {
        case SectionPicture: {
            
            
//            [self openAccount];
            
            
            break;
        }
            
        case SectionsViews:{
            
            //NSLog(@"Class %@", self.mm_drawerController.centerViewController.navigationController.visibleViewController.class);
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            int Path = -1;
            
            if(indexPath.row != 5+Path){
                _selectIndexPath = indexPath;
            }
            if(indexPath.row == Path){
                
//                 _editCurrentVC = [storyboard instantiateViewControllerWithIdentifier:@"currentCard"];
//                [self.mm_drawerController
//                 setCenterViewController:_editCurrentVC
//                 withCloseAnimation:YES
//                 completion:nil];
            }
            else if(indexPath.row == 1+Path){

                if (!_homesVC) {
                    _homesVC = [storyboard instantiateViewControllerWithIdentifier:@"homes"];
                }

                [self.mm_drawerController
                 setCenterViewController:_homesVC
                 withCloseAnimation:YES
                 completion:nil];
            }
            else if(indexPath.row == 2+Path){
                
                if (!_roomesVC) {
                    _roomesVC = [storyboard instantiateViewControllerWithIdentifier:@"roomes"];
                }
                
                [self.mm_drawerController
                 setCenterViewController:_roomesVC
                 withCloseAnimation:YES
                 completion:nil];
            }
            else if(indexPath.row == 3+Path){
                
                if (!_zonesVC) {
                    _zonesVC = [storyboard instantiateViewControllerWithIdentifier:@"zones"];
                }
                
                [self.mm_drawerController
                 setCenterViewController:_zonesVC
                 withCloseAnimation:YES
                 completion:nil];
            }
            else if(indexPath.row == 4+Path){
                if (!_scenesVC) {
                    _scenesVC = [storyboard instantiateViewControllerWithIdentifier:@"scenes"];
                }
                
                [self.mm_drawerController
                 setCenterViewController:_scenesVC
                 withCloseAnimation:YES
                 completion:nil];

            }
            else if(indexPath.row == 5+Path){
                if (!_triggerVC) {
                    _triggerVC = [storyboard instantiateViewControllerWithIdentifier:@"trigger"];
                }
                
                [self.mm_drawerController
                 setCenterViewController:_triggerVC
                 withCloseAnimation:YES
                 completion:nil];
                
            
            }
            else if(indexPath.row == 6+Path){
                if (!_moreVC) {
                    _moreVC = [storyboard instantiateViewControllerWithIdentifier:@"more"];
                }
                
                [self.mm_drawerController
                 setCenterViewController:_moreVC
                 withCloseAnimation:YES
                 completion:nil];
                
            }

    
            break;
        }
            
        default:
            
            
            break;
    }
    


    
    
    [self.tableView reloadData];
}

//- (void)openCredits {
//    if ([PMOauth hasAccessToAPI:YES]) {
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        
//        _guthabenAufladenVC = [storyboard instantiateViewControllerWithIdentifier:@"helpViewVC"];
//        _guthabenAufladenVC.urlToShow = [NSURL URLWithString:S_URL_buyCredits2];
//        _guthabenAufladenVC.buyCredits = YES;
//        _guthabenAufladenVC.buyCreditsLeftMenu = YES;
//        
//        NavController *navCredits = [[NavController alloc] initWithRootViewController:_guthabenAufladenVC];
//        
//        [self.mm_drawerController presentViewController:navCredits animated:YES completion:^{
//            
//        }];
//        
//    }
//    else {
//        
//        NXOAuth2Account *acc = (NXOAuth2Account *)[[[NXOAuth2AccountStore sharedStore] accounts] firstObject];
//        if ([acc.accessToken hasExpired]) {
//            
//            [self performSelector:@selector(openCredits) withObject:nil afterDelay:1];
//            return;
//        }
//        [PMOauth openLoginNativ];
//    }
//
//}
//
//- (void)openAccount {
//    if ([PMOauth hasAccessToAPI:YES]) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
////        if (!_accountVC) {
//            _accountVC = [storyboard instantiateViewControllerWithIdentifier:@"accountView"];
////        }
//        
//        [self.mm_drawerController presentViewController:_accountVC animated:YES completion:^{
//            
//        }];
//    }
//    else {
//        
//        NXOAuth2Account *acc = (NXOAuth2Account *)[[[NXOAuth2AccountStore sharedStore] accounts] firstObject];
//        if ([acc.accessToken hasExpired]) {
//            
//            [self performSelector:@selector(openAccount) withObject:nil afterDelay:1];
//            return;
//        }
//        [PMOauth openLoginNativ];
//    }
//}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)refreshAccountInfo {
//    [PMOauth profileInformation:YES complete:^{

        [self.tableView reloadData];
//    }];
    
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end