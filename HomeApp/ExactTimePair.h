//
//  ExactTimePair.h
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TimeConditionViewController.h"
#import <Foundation/Foundation.h>

@interface ExactTimePair : NSObject


@property (nonatomic, assign) TimeConditionOrder timeOrder;
@property (nonatomic, assign) NSDateComponents *dateComponents;



-(id)init:(TimeConditionOrder)timeOrder withDateComponents:(NSDateComponents*)dateComponents;

@end
