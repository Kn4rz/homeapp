//
//  EditActionTriggerViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 11.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditActionTriggerViewController.h"
#import "EditActionViewController.h"
#import "ChooseAccessoryViewController.h"
#import "TableViewNameCell.h"
#import "TableViewSwitchCell.h"
#import "CharacteristicStringFormatter.h"
#import "HomeKitManager.h"
#import <HomeKit/HomeKit.h>

@interface EditActionTriggerViewController () {

    NSMutableArray *events;
}
@end


@implementation EditActionTriggerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"CharacteristicChoosed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateEvents) name:@"EventUpdated" object:nil];
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    [super loadData];
    
    events = self.isAdding ? [[NSMutableArray alloc] init] : [[NSMutableArray alloc] initWithArray:self.eventTrigger.events];
    
    [self.tableView reloadData];
}


#pragma mark - Notification

- (void)receiveNotification:(NSNotification *) notification {
    
    NSDictionary *userInfo = notification.userInfo;
    HMCharacteristic *choosedCharacteristic = [userInfo objectForKey:@"characteristic"];
    NSNumber *value = [userInfo objectForKey:@"characteristic-value"];
    
    NSLog(@"%@", choosedCharacteristic);
    [self createNewEvent:choosedCharacteristic value:value];
}

- (void)updateEvents {
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:2];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    
    NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    if(self.isAdding) {
        self.saveButton.enabled = (trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName]);
        
    } else {
        self.saveButton.enabled = true;
    }
}

-(void)saveButtonPressed {
    
    if(self.isAdding) {
        
        HMEventTrigger *trigger = [self newTrigger];
        
        [[HomeKitManager sharedManager] addTrigger:trigger toHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error) {
            
            if(error) {
                NSLog(@"Error adding action trigger %@", error);
                
            } else {
                
                [self addAllSelectedActionSetsToTrigger:trigger];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self enableTrigger:trigger];
                });
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
        
    } else {
        
        NSString *trimmedName =[self.nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        if(trimmedName.length > 0 && ![self.trigger.name isEqualToString:trimmedName])
            [self updateTriggerName:trimmedName];
        
        
        if((events != nil && events.count > 0)
           && events.count != self.eventTrigger.events.count) {
            
            [self removeAllEventsFromTrigger:self.eventTrigger];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addAllEventsToTrigger:self.eventTrigger];
            });
        }
        
        
        [self updateEventTriggerConditions:self.eventTrigger];
        
        
        if((self.selectedActionSets != nil && self.selectedActionSets.count > 0)
           && self.selectedActionSets.count != self.trigger.actionSets.count) {
            
            [self removeAllActionSetsFromTrigger:self.trigger];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addAllSelectedActionSetsToTrigger:self.trigger];
            });
        }
        
        
        if(self.trigger.isEnabled != self.enabledSwitch.on)
            [self enableTrigger:self.trigger];
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - New Event Trigger creation

-(NSArray*)createEventsForEventTrigger {
    return [events mutableCopy];
}


#pragma mark - Event creation

-(void)createNewEvent:(HMCharacteristic*)characteristic value:(NSNumber*)value {
    
    HMCharacteristicEvent *newEvent = [[HMCharacteristicEvent alloc] initWithCharacteristic:characteristic triggerValue:value];
    [events addObject:newEvent];
    
    [self enableSaveButtonIfNecessary];
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:2];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Event delete methods

-(void)deleteEvent:(HMEvent*)event {
    [self.eventTrigger removeEvent:event completionHandler:^(NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Error deleting event from event trigger %@", error);
        
        } else {
        
            NSLog(@"Event deleted from event trigger");
            
            [events removeObject:event];
            NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:2];
            [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}


#pragma mark - Trigger update methods

-(void)removeAllEventsFromTrigger:(HMEventTrigger*)trigger {
    for (HMEvent *event in trigger.events) {
        [trigger removeEvent:event completionHandler:^(NSError * _Nullable error) {
            
            if(error) {
                NSLog(@"Error removing event on action trigger %@", error);
                
            } else {
                NSLog(@"Action Trigger's event removed");
            }
        }];
    }
}

-(void)addAllEventsToTrigger:(HMEventTrigger*)trigger {
    for (HMEvent *event in events) {
        [trigger addEvent:event completionHandler:^(NSError * _Nullable error) {
            
            if(error) {
                NSLog(@"Error ading event on action trigger %@", error);
                
            } else {
                NSLog(@"Event added on action trigger");
            }
        }];
    }
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
            
        case Characteristic:
            return @"Characteristics";
            
        default:
            return [super tableView:tableView titleForHeaderInSection:section];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case Characteristic:
            return events.count + 1;
            
        default:
            return [super tableView:tableView numberOfRowsInSection:section];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Characteristic:
            
            switch ([self cellTypeForIndexPath:indexPath]) {
                case Add:
                    return [self addCellForRowAtIndexPath:indexPath];
                
                case Object:
                    return [self actionCellForRowAtIndexPath:indexPath];
                    
                default:
                    return nil;
            }
            
        default:
            return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self sectionForIndex:indexPath.section]) {
            
        case Characteristic:
            return !([self cellTypeForIndexPath:indexPath] == Add);
            
        default:
            return [super tableView:tableView canEditRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        switch ([self sectionForIndex:indexPath.section]) {
            case Characteristic:
                [self deleteEvent:[events objectAtIndex:indexPath.row]];
                break;
                
            default:
                [super tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
                break;
        }
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Characteristic:
            
            if([self cellTypeForIndexPath:indexPath] == Add) {
                [self performSegueWithIdentifier:@"Add New Event" sender:self];
            }
           
            break;
            
        default:
            [super tableView:tableView didSelectRowAtIndexPath:indexPath];
            break;
    }
}


#pragma mark - TableView datasource - Helper

-(int)sectionForConditions {
    return 3;
}

-(TriggerTableViewSection)sectionForIndex:(NSInteger)section {
    switch (section) {
        case 0:
            return Name;
            
        case 1:
            return Enabled;
            
        case 2:
            return Characteristic;
            
        case 3:
            return Conditions;
            
        case 4:
            return ActionSets;
            
        default:
            return Unknown;
    }
}

-(NSString*)titleForAddRowInSection:(NSIndexPath*)indexPath {
    switch ([self sectionForIndex:indexPath.section]) {
        case Characteristic:
            return @"Add Event…";
            
        default:
            return [super titleForAddRowInSection:indexPath];
    }
}

-(UITableViewCell*)actionCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ActionCell"];
    
    HMCharacteristicEvent *event = [events objectAtIndex:indexPath.row];
    HMService *service = event.characteristic.service;
    HMAccessory *accessory = service.accessory;
    
    cell.textLabel.text = [CharacteristicStringFormatter localizedStringFromCharacteristic:event.characteristic withValue:event.triggerValue];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ in %@", service.name, accessory.name];
    return cell;
}

-(NSObject*) objectsForSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case Characteristic:
            return events;
            
        default:
            return [super objectsForSection:section];
    }
}


#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"Edit Event"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        HMEvent *event = [events objectAtIndex:indexPath.row];
        if([self.eventTrigger.events containsObject:event]) {
            return YES;
            
        } else {
            return NO;
        }
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Edit Event"]) {
            EditActionViewController *acvvc = (EditActionViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            
            HMCharacteristicEvent *event = [events objectAtIndex:indexPath.row];
            acvvc.selectedEvent = event;
        }
    } else {
        
        if ([segue.identifier isEqualToString:@"Add New Event"]) {
            ChooseAccessoryViewController *vc = (ChooseAccessoryViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            vc.onlyShowsControlServices = true;
        }
    }

}

@end
