//
//  AccessoriesViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "AccessoriesViewController.h"
#import "ServicesViewController.h"
#import "EditAccessoryViewController.h"
#import "HomeKitManager.h"

@interface AccessoriesViewController () {
    NSMutableArray *accessories;
}
@end


@implementation AccessoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Accessories";
    self.objectType = Accessory;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    accessories = [[NSMutableArray alloc] init];
    for(HMAccessory *accessory in [HomeKitManager sharedManager].currentHome.accessories) {
        accessory.delegate = self;
        [accessories addObject:accessory];
    }
    [self.tableView reloadData];
}


#pragma mark - Accessory delete methods

-(void)deleteAccessory:(HMAccessory*)accessory {
    [[HomeKitManager sharedManager] removeAccessory:accessory fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Accessory from Home");
        
        [accessories removeObject:accessory];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateReachability:(HMAccessory *)accessory {
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - TableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return accessories.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self cellTypeForIndexPath:indexPath]) {
        case Add:
            return [self addCellForRowAtIndexPath:indexPath];
            
        case Object:
            return [self homeKitObjectCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return !([self cellTypeForIndexPath:indexPath] == Add);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self deleteAccessory:[accessories objectAtIndex:indexPath.row]];

    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        [self performSegueWithIdentifier:@"Add Accessory" sender:self];
    }
}


#pragma mark - TableView datasource - Helper

-(UITableViewCell*)homeKitObjectCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    HMAccessory *accessory = [accessories objectAtIndex:indexPath.row];
    NSString *name = accessory.name;
    
    NSString *reuseIdentifier = accessory.reachable ? @"AccessoryCell" : @"UnreachableAccessoryCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = name;
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Show Accessory"]) {
            ServicesViewController *avc = (ServicesViewController*)[segue destinationViewController];
            avc.selectedAccessory = [accessories objectAtIndex:indexPath.row];
        }
        
        if ([segue.identifier isEqualToString:@"Modify Accessory"]) {
            EditAccessoryViewController *eavc = (EditAccessoryViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            eavc.accessory = [accessories objectAtIndex:indexPath.row];
        }
        
    }
}

@end
