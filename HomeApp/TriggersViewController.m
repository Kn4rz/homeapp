//
//  TriggersViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TriggersViewController.h"
#import "EditTimerTriggerViewController.h"
#import "EditLocationTriggerViewController.h"
#import "EditActionTriggerViewController.h"
#import "HomeKitManager.h"

@interface TriggersViewController () {
    NSMutableArray *triggers;
}
@end


@implementation TriggersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Triggers";
    self.objectType = Trigger;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    triggers = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.triggers];
    [self.tableView reloadData];
}


#pragma mark - Trigger creation methods

-(void)showNewTriggerView {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Add Trigger" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Time" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Add Timer Trigger" sender:self];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Add Location Trigger" sender:self];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Action" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSegueWithIdentifier:@"Add Action Trigger" sender:self];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}


#pragma mark - Location Trigger checker method

-(bool)isLocationTrigger:(HMEventTrigger*)trigger {
    for (HMEvent *event in trigger.events) {
        if([event isKindOfClass:[HMLocationEvent class]]) {
            return true;
        }
    }
    return false;
}

#pragma mark - Trigger delete methods

-(void)deleteTrigger:(HMTrigger*)trigger {
    [[HomeKitManager sharedManager] removeTrigger:trigger fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Trigger from Home");
        
        [triggers removeObject:trigger];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}


#pragma mark - TableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return triggers.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self cellTypeForIndexPath:indexPath]) {
        case Add:
            return [self addCellForRowAtIndexPath:indexPath];
            
        case Object:
            return [self homeKitObjectCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return !([self cellTypeForIndexPath:indexPath] == Add);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        [self deleteTrigger:[triggers objectAtIndex:indexPath.row]];

    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        [self showNewTriggerView];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    HMTrigger *trigger = [triggers objectAtIndex:indexPath.row];
    if([trigger isKindOfClass:[HMTimerTrigger class]]) {
        
        [self performSegueWithIdentifier:@"Show Timer Trigger" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
        
    } else {
        
        if([self isLocationTrigger:(HMEventTrigger*)trigger]) {
            [self performSegueWithIdentifier:@"Show Location Trigger" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
            
        } else {
            [self performSegueWithIdentifier:@"Show Action Trigger" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
        }
    }
}


#pragma mark - TableView datasource - Helper

-(UITableViewCell*)homeKitObjectCellForRowAtIndexPath:(NSIndexPath*)indexPath {

    HMTrigger *trigger = [triggers objectAtIndex:indexPath.row];
    NSString *name = trigger.name;
        
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[self reuseIdentifierForCellForHomeKitObject:Trigger]];
    cell.textLabel.text = name;
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Timer Trigger"]) {
        EditTimerTriggerViewController *svc = (EditTimerTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        svc.isAdding = true;
    }
    
    if ([segue.identifier isEqualToString:@"Show Timer Trigger"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        EditTimerTriggerViewController *svc = (EditTimerTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        svc.trigger = [triggers objectAtIndex:indexPath.row];
    }
    
    if ([segue.identifier isEqualToString:@"Add Location Trigger"]) {
        EditLocationTriggerViewController *vc = (EditLocationTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        vc.isAdding = true;
    }
    
    if ([segue.identifier isEqualToString:@"Show Location Trigger"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        EditLocationTriggerViewController *vc = (EditLocationTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        vc.trigger = [triggers objectAtIndex:indexPath.row];
    }
    
    if ([segue.identifier isEqualToString:@"Add Action Trigger"]) {
        EditActionTriggerViewController *vc = (EditActionTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        vc.isAdding = true;
    }
    
    if ([segue.identifier isEqualToString:@"Show Action Trigger"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        EditActionTriggerViewController *vc = (EditActionTriggerViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
        vc.trigger = [triggers objectAtIndex:indexPath.row];
    }
}

@end
