//
//  TimeConditionViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TableViewDatePickerCell.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TimeConditionTableViewSection) {
    
    TimeOrSun = 0,
    
    BeforeOrAfter = 1,
    
    Value = 2,
    
    UnknownSection = 3
};

typedef NS_ENUM(NSInteger, TimeConditionType) {
    
    Time = 0,
    
    Sun = 1,
};

typedef NS_ENUM(NSInteger, TimeConditionSunState) {
    
    Sunrise = 0,
    
    Sunset = 1
};

typedef NS_ENUM(NSInteger, TimeConditionOrder) {
    
    Before = 0,
    
    After = 1,
    
    At = 2
};

@interface TimeConditionViewController : UITableViewController <TableViewDatePickerCellDelegate>

@end
