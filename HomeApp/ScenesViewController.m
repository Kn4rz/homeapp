//
//  ScenesViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "ScenesViewController.h"
#import "EditSceneViewController.h"
#import "HomeKitManager.h"

@interface ScenesViewController () {
    NSMutableArray *actionSets;
}
@end


@implementation ScenesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Scenes";
    self.objectType = ActionSet;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    actionSets = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.actionSets];
    [self.tableView reloadData];
}


#pragma mark - ActionSet delete methods

-(void)deleteActionSet:(HMActionSet*)actionSet {
    [[HomeKitManager sharedManager] removeActionSet:actionSet fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Scene from Home");
        
        [actionSets removeObject:actionSet];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}


#pragma mark - TableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return actionSets.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self cellTypeForIndexPath:indexPath]) {
        case Add:
            return [self addCellForRowAtIndexPath:indexPath];
            
        case Object:
            return [self homeKitObjectCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row != actionSets.count) {
        HMActionSet *actionSet = [actionSets objectAtIndex:indexPath.row];
        bool isBuiltInType = ![actionSet.actionSetType isEqualToString:HMActionSetTypeUserDefined];
        
        return !(isBuiltInType || [self cellTypeForIndexPath:indexPath] == Add);
    }
    return !([self cellTypeForIndexPath:indexPath] == Add);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self deleteActionSet:[actionSets objectAtIndex:indexPath.row]];

    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        [self performSegueWithIdentifier:@"Add Action Set" sender:self];
    }
}


#pragma mark - TableView datasource - Helper

-(UITableViewCell*)homeKitObjectCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    HMActionSet *actionSet = [actionSets objectAtIndex:indexPath.row];
    NSString *name = actionSet.name;

    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[self reuseIdentifierForCellForHomeKitObject:ActionSet]];
    cell.textLabel.text = name;
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Show Action Set"]) {
            EditSceneViewController *svc = (EditSceneViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            svc.actionSet = [actionSets objectAtIndex:indexPath.row];
        }

    } else {
        
        if ([segue.identifier isEqualToString:@"Add Action Set"]) {
            EditSceneViewController *svc = (EditSceneViewController*)[[((UINavigationController *)segue.destinationViewController) viewControllers] firstObject];
            svc.isAdding = true;
        }
    }
}

@end
