//
//  AddNewAccesoryViewController.h
//  HomeKit_KnarzSample
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface AddNewAccesoryViewController : UITableViewController <HMAccessoryBrowserDelegate, UITableViewDelegate, UITableViewDataSource>


@end