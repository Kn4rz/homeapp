//
//  CharacteristicsViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

typedef NS_ENUM(NSInteger, CharacteristicTableViewSection) {
    
    Characteristics = 0,
    
    AssociatedServiceType = 1,
    
    UnknownCharacteristicSection = 2
};



@interface CharacteristicsViewController : UITableViewController <HMAccessoryDelegate>

@property (strong, nonatomic) HMService *selectedService;

@end
