//
//  EditActionViewController.h
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditActionViewController : UITableViewController

@property (strong, nonatomic) HMCharacteristicWriteAction *selectedAction;
@property (strong, nonatomic) HMCharacteristicEvent *selectedEvent;

@end
