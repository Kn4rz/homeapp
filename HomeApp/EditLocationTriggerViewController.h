//
//  EditLocationTriggerViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "MapViewController.h"
#import "EditEventTriggerViewController.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>
#import <MapKit/MapKit.h>

@interface EditLocationTriggerViewController : EditEventTriggerViewController <MapViewControllerDelegate, MKMapViewDelegate>



@end
