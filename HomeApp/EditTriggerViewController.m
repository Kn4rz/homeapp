//
//  EditTriggerViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditTriggerViewController.h"
#import "TableViewNameCell.h"
#import "TableViewSwitchCell.h"
#import "HomeKitManager.h"
#import <HomeKit/HomeKit.h>

@interface EditTriggerViewController () {
    
}
@end


@implementation EditTriggerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NameCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NameCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SwitchCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SwitchCell"];
    
    
    if(self.trigger != nil) {
        self.title = self.trigger.name;
        [self nameFieldDidChanged];
    }
    
    self.saveButton = [[UIBarButtonItem alloc] init];
    self.saveButton.title = @"Save";
    self.saveButton.enabled = NO;
    [self.saveButton setAction:@selector(saveButtonPressed)];
    [self.saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = self.saveButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
}


#pragma mark - Loading Data

-(void)loadData {
    self.actionSets = [[NSMutableArray alloc] initWithArray:[self selectableActionSets]];
    self.selectedActionSets = self.isAdding ? [[NSMutableArray alloc] init] : [[NSMutableArray alloc] initWithArray:self.trigger.actionSets];
}

-(NSArray*)selectableActionSets {
    NSMutableArray *filteredActionSets = [[NSMutableArray alloc] init];
    for (HMActionSet *actionSet in [HomeKitManager sharedManager].currentHome.actionSets) {
        
        if(actionSet.actions.count > 0 && actionSet.actions != nil) {
            [filteredActionSets addObject:actionSet];
        }
    }
    return filteredActionSets;
}


#pragma mark - Button Actions

-(void)enableSaveButtonIfNecessary {
    return;
}

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveButtonPressed {
    return;
}


#pragma mark - Trigger update methods

-(void)updateTriggerName:name {
    [self.trigger updateName:name completionHandler:^(NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Error renaming trigger %@", error);
            
        } else {
            NSLog(@"Trigger renamed");
        }
    }];
}

-(void)enableTrigger:(HMTrigger*)trigger {
    [trigger enable:self.enabledSwitch.on completionHandler:^(NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Error enabling trigger %@", error);
            
        } else {
            NSLog(@"Trigger enable status updated");
        }
    }];
}

-(void)removeAllActionSetsFromTrigger:(HMTrigger*)trigger {
    for (HMActionSet *actionSet in trigger.actionSets) {
        [trigger removeActionSet:actionSet completionHandler:^(NSError * _Nullable error) {
            
            if(error) {
                NSLog(@"Error removing actionSet on trigger %@", error);
                
            } else {
                NSLog(@"Trigger's actionSet removed");
            }
        }];
    }
}

-(void)addAllSelectedActionSetsToTrigger:(HMTrigger*)trigger {
    for (HMActionSet *actionSet in self.selectedActionSets) {
        [trigger addActionSet:actionSet completionHandler:^(NSError * _Nullable error) {
            
            if(error) {
                NSLog(@"Error ading actionSet on trigger %@", error);
                
            } else {
                NSLog(@"ActionSet added on trigger");
            }
        }];
    }
}


#pragma mark - TableViewNameCell Delegate

-(void)nameFieldDidChanged {
    [self enableSaveButtonIfNecessary];
}


#pragma mark - TableViewSwitchCell Delegate

-(void)switchDidChanged {
    [self enableSaveButtonIfNecessary];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case Name:
            return nil;
            
        case Enabled:
            return nil;

        case ActionSets:
            return @"Scenes";
            
        default:
            return @"undefined";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case ActionSets:
            return self.actionSets.count;
            
        default:
            return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case Name:
            return [self nameCellForRowAtIndexPath:indexPath];
            
        case Enabled:
            return [self switchCellForRowAtIndexPath:indexPath];
            
        case ActionSets:
            return [self actionSetCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case ActionSets:
            [self didSelectActionSetAtIndexPath:indexPath];
            break;
            
        default:
            break;
    }
}


#pragma mark - TableView datasource Helper

-(TriggerTableViewSection)sectionForIndex:(NSInteger)section {
    return Unknown;
}
    
-(TableViewNameCell*)nameCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    TableViewNameCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NameCell"];
    if (cell == nil) {
        cell = [[TableViewNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
    }
    self.nameField = cell.nameField;
    cell._delegate = self;
    
    cell.nameField.text = self.trigger.name;
    return cell;
}

-(TableViewSwitchCell*)switchCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    TableViewSwitchCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
    if (cell == nil) {
        cell = [[TableViewSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SwitchCell"];
    }
    self.enabledSwitch = cell.enabledSwitch;
    cell._delegate = self;
    
    cell.enabledSwitch.on = self.trigger.isEnabled;
    return cell;
}

-(UITableViewCell*)actionSetCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ActionSetCell"];
    
    HMActionSet *actionSet = [self.actionSets objectAtIndex:indexPath.row];
    if ([self.selectedActionSets containsObject:actionSet])  {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = actionSet.name;
    return cell;
}

-(void)didSelectActionSetAtIndexPath:(NSIndexPath*)indexPath {
    
    HMActionSet *actionSet = [self.actionSets objectAtIndex:indexPath.row];
    if([self.selectedActionSets containsObject:actionSet]) {
        [self.selectedActionSets removeObject:actionSet];
        
    } else {
        [self.selectedActionSets addObject:actionSet];
    }
    
    [self enableSaveButtonIfNecessary];
//    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
//    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
