//
//  HomeKitEnums.h
//  HomeApp
//
//  Created by Markus Drechsel on 11.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

typedef NS_ENUM(NSInteger, HomeCellType) {
    Object = 0,
    Add = 1,
    None = 2
};

typedef NS_ENUM(NSInteger, HomeKitObjectSection) {
    Accessory = 0,
    Room = 1,
    Zone = 2,
    ActionSet = 3,
    Trigger = 4
    
};