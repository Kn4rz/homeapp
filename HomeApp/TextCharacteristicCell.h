//
//  TextCharacteristicCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "CharacteristicCell.h"

@interface TextCharacteristicCell : CharacteristicCell

@property (weak, nonatomic) IBOutlet UITextField *textField;


@end
