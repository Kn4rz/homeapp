//
//  TableViewNameCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//


#import "TableViewNameCell.h"


@implementation TableViewNameCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    
    self.nameField.delegate = self;
    
}

- (void)prepareForReuse {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (UITableView *)styleTableView:(UITableView *)_tableView forId:(NSString *)cellId {
    
    
    
    return _tableView;
    
}

- (IBAction)nameFieldDidChanged:(id)sender {
    [self._delegate nameFieldDidChanged];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
