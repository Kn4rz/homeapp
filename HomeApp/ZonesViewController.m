//
//  ZonesViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "ZonesViewController.h"
#import "EditZoneViewController.h"
#import "HomeKitManager.h"

@interface ZonesViewController () {
    NSMutableArray *zones;
}
@end


@implementation ZonesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Zones";
    self.objectType = Zone;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    zones = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.zones];
    [self.tableView reloadData];
}


#pragma mark - Zones creation methods

-(void)showNewZoneView {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add New Zone" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
        
        [[HomeKitManager sharedManager] addZone:textField.text toHome:[HomeKitManager sharedManager].currentHome completion:^(HMZone *zone, NSError *error) {
            if (error) {
                NSLog(@"Error adding new zone: %@", error);
            } else {
                NSLog(@"New zone added.");
                [self addZone:zone];
            }
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)addZone:(HMZone*)zone {
    NSMutableArray *newZones = [NSMutableArray arrayWithArray:zones];
    [newZones addObject:zone];
    zones = [NSMutableArray arrayWithArray:newZones];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Zone delete methods

-(void)deleteZone:(HMZone*)zone {
    [zones removeObject:zone];
    [[HomeKitManager sharedManager] removeZone:zone fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Room from Home");
        
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}


#pragma mark - TableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return zones.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self cellTypeForIndexPath:indexPath]) {
        case Add:
            return [self addCellForRowAtIndexPath:indexPath];
            
        case Object:
            return [self homeKitObjectCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return !([self cellTypeForIndexPath:indexPath] == Add);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        [self deleteZone:[zones objectAtIndex:indexPath.row]];
        
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        [self showNewZoneView];
    }
}


#pragma mark - TableView datasource - Helper

-(UITableViewCell*)homeKitObjectCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    HMZone *zone = [zones objectAtIndex:indexPath.row];
    NSString *name = zone.name;
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[self reuseIdentifierForCellForHomeKitObject:Zone]];
    cell.textLabel.text = name;
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Show Zone"]) {
            EditZoneViewController *zvc = (EditZoneViewController*)[segue destinationViewController];
            zvc.selectedZone = [zones objectAtIndex:indexPath.row];
        }
    }
}

@end
