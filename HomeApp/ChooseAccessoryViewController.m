//
//  ChooseAccessoryViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "ChooseAccessoryViewController.h"
#import "ChooseServiceViewController.h"
#import "HomeKitManager.h"

@interface ChooseAccessoryViewController () {
    NSMutableArray *displayedAccessories;
}
@end

@implementation ChooseAccessoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Choose Accessory";
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    displayedAccessories = [[NSMutableArray alloc] initWithArray:[self filterAccessories]];
    [self.tableView reloadData];
}

-(NSArray*)filterAccessories {
    NSMutableArray *filteredAccessories = [[NSMutableArray alloc] init];
    for (HMAccessory *accessory in [HomeKitManager sharedManager].currentHome.accessories) {
        accessory.delegate = self;
        if(accessory.reachable) {
            
            bool isControlAccessory = false;
            for(HMService *service in accessory.services) {
                if(!([service.serviceType isEqualToString:HMServiceTypeAccessoryInformation] ||
                     [service.serviceType isEqualToString:HMServiceTypeLockManagement]) ){
                    
                    isControlAccessory = true;
                }
            }
            
            if(isControlAccessory)
                [filteredAccessories addObject:accessory];
        }
    }
    return filteredAccessories;
}


#pragma mark - Button Actions

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - HMAccessory Delegate

- (void)accessoryDidUpdateReachability:(HMAccessory *)accessory {
    [self loadData];
}

- (void)accessoryDidUpdateServices:(HMAccessory *)accessory {
    [self loadData];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return displayedAccessories.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"AccessoryCell"];
    
    HMRoom *room = [displayedAccessories objectAtIndex:indexPath.row];
    cell.textLabel.text = room.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    
    ChooseServiceViewController *csvc = (ChooseServiceViewController*)[segue destinationViewController];
    csvc.selectedAccessory = [displayedAccessories objectAtIndex:indexPath.row];
    csvc.isCondition = self.isCondition;
    csvc.onlyShowsControlServices = self.onlyShowsControlServices;
}

@end
