//
//  AddCharacteristicValueViewController.m
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import "AppDelegate.h"
#import "AddCharacteristicValueViewController.h"

@interface AddCharacteristicValueViewController (){
    NSNumber *value;
}

@end

@implementation AddCharacteristicValueViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.isAdding ? @"Add Value" : @"Edit Characteristic";
    
    value = [NSNumber numberWithInt:0];
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] init];
    addButton.title = @"Add";
    [addButton setAction:@selector(addButtonPressed)];
    [addButton setTarget:self];
    self.navigationItem.rightBarButtonItem = self.isAdding ? addButton : saveButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


-(void)addButtonPressed {
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:self.selectedCharacteristic forKey:@"characteristic"];
    [userInfo setValue:value forKey:@"characteristic-value"];

    if(self.isCondition) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewActionConditionCreated" object:nil userInfo:userInfo];
        
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CharacteristicChoosed" object:nil userInfo:userInfo];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveButtonPressed {

    
    
}

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)changeValue:(id)sender{
    
    if([sender isKindOfClass:[UISlider class]]) {
        UISlider *slider = (UISlider*) sender;
        NSLog(@"%f", slider.value);
        value = [NSNumber numberWithFloat:slider.value];
    }
    
    if([sender isKindOfClass:[UISwitch class]]) {
        UISwitch *switchControl = (UISwitch*) sender;
        NSLog(@"%d", switchControl.on);
        value = [NSNumber numberWithBool:switchControl.on];
    }
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Add Value for Characteristic";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HMCharacteristic *characteristic = self.selectedCharacteristic;
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CharacteristicCell"];
    cell.textLabel.text = characteristic.localizedDescription;

    if ([characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetLockMechanismState] || [characteristic.characteristicType isEqualToString:HMCharacteristicTypePowerState])
    {
        
        BOOL lockState = [characteristic.value boolValue];
        
        UISwitch *lockSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        lockSwitch.on = lockState;
        [lockSwitch addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = lockSwitch;
        
        value = [NSNumber numberWithBool:lockState];
        
    } else if ([characteristic.characteristicType isEqualToString:HMCharacteristicTypeSaturation] ||
               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeBrightness] ||
               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeHue] ||
               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetTemperature] ||
               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeTargetRelativeHumidity] ||
               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeCoolingThreshold] ||
               [characteristic.characteristicType isEqualToString:HMCharacteristicTypeHeatingThreshold])
    {
        UISlider *slider = [[UISlider alloc] init];
        slider.bounds = CGRectMake(0, 0, 125, slider.bounds.size.height);
        slider.maximumValue = [characteristic.metadata.maximumValue floatValue];
        slider.minimumValue = [characteristic.metadata.minimumValue floatValue];
        slider.value = [characteristic.value floatValue];
        slider.continuous = true;
        [slider addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
        
        cell.accessoryView = slider;
        
        value = [NSNumber numberWithBool:slider.value];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    HMCharacteristic *characteristic = [charateristics objectAtIndex:indexPath.row];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"CharacteristicChoosed" object:characteristic];
//    
//    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
