//
//  TextCharacteristicCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TextCharacteristicCell.h"

@implementation TextCharacteristicCell


-(void)setCharacteristicValue:(HMCharacteristic*)characteristic {
    [super setCharacteristicValue:characteristic];
    
    self.textField.text = characteristic.value;
}

- (IBAction)textFieldDidEndEditiing:(id)sender {
    
    
    [self.characteristic writeValue:self.textField.text completionHandler:^(NSError *error){
        
        if(error == nil){
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self updateValue:self.characteristic.value];
            });
        } else {
            NSLog(@"error in writing characterstic: %@",error);
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
