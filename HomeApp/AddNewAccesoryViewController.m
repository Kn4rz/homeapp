//
//  AddNewAccesoryViewController.m
//  SampleHomeKit
//
//  Created by Porwal, Animesh on 7/3/14.
//  Copyright (c) 2014 AP. All rights reserved.
//

#import "AddNewAccesoryViewController.h"
#import "EditAccessoryViewController.h"
#import "HomeKitManager.h"

@interface AddNewAccesoryViewController () {
    NSMutableArray *displayedAccessories;
}

@end

@implementation AddNewAccesoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[HomeKitManager sharedManager] setAccesoryBrowserDelegate:self];
    
    displayedAccessories = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] init];
    doneButton.title = @"Done";
    [doneButton setAction:@selector(doneButtonPressed)];
    [doneButton setTarget:self];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[HomeKitManager sharedManager] startSearchingForNewAccessories];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

-(void)loadData {
    displayedAccessories = [[NSMutableArray alloc] initWithArray:[[HomeKitManager sharedManager] getDiscoverdAccessories]];
    [self.tableView reloadData];
}


-(void)doneButtonPressed {
    [[HomeKitManager sharedManager] stopSearchingForNewAccessories];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - TableView datasource and delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return displayedAccessories.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccessoryCell" forIndexPath:indexPath];
    cell.textLabel.text = [[displayedAccessories objectAtIndex:indexPath.row] name];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    [[HomeKitManager sharedManager] addAccessory:[displayedAccessories objectAtIndex:indexPath.row] toHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
//        
//        if (error) {
//            NSLog(@"error in adding accessory: %@", error);
//        }
//    }];
    
//    if ([displayedAccessories containsObject:[displayedAccessories objectAtIndex:indexPath.row]]) {
        [self performSegueWithIdentifier:@"Add Accessory" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
//    }
    
}

#pragma mark - HMAccessoryBrowserDelegate

- (void)accessoryBrowser:(HMAccessoryBrowser *)browser didFindNewAccessory:(HMAccessory *)accessory {
    
    [displayedAccessories addObject:accessory];
    [self.tableView reloadData];
}

-(void)accessoryBrowser:(HMAccessoryBrowser *)browser didRemoveNewAccessory:(HMAccessory *)accessory {
    
    [displayedAccessories removeObject:accessory];
    [self.tableView reloadData];
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
     if([segue.identifier isEqualToString:@"Add Accessory"]) {
         EditAccessoryViewController *eavc = (EditAccessoryViewController*)segue.destinationViewController;
         eavc.accessory = [displayedAccessories objectAtIndex:indexPath.row];
         eavc.isAdding = true;
     }
 }

@end
