//
//  CharacteristicCell.m
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "CharacteristicCell.h"
#import "CharacteristicStringFormatter.h"

@implementation CharacteristicCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)setCharacteristicValue:(HMCharacteristic*)characteristic {
    self.characteristic = characteristic;
    
    self.typeLabel.text = [CharacteristicStringFormatter localizedStringForCharacteristicType:self.characteristic];
    
    [self updateValue:self.characteristic.value];
    
    self.typeLabel.alpha = characteristic.service.accessory.reachable ? 1.0 : 0.4;
    self.valueLabel.alpha  = characteristic.service.accessory.reachable ? 1.0 : 0.4;
}


-(void)updateValue:(id)value {
    self.value = value;
    
    if(self.value != nil) {
        [self resetValueLabel];
    }
}

-(void)resetValueLabel {
    if(self.value != nil) {
        self.valueLabel.text = [CharacteristicStringFormatter localizedStringForCharacteristicValue:self.value withCharacteristic:self.characteristic];
    }
}

@end
