//
//  CharacteristicCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 16.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface CharacteristicCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *favoriteButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *favoriteButtonHeightConstraint;


@property (nonatomic, assign) bool showsFavorites;

@property (nonatomic, assign) id value;
@property (nonatomic, assign) HMCharacteristic *characteristic;



-(void)setCharacteristicValue:(HMCharacteristic*)characteristic;
-(void)updateValue:(id)value;

@end
