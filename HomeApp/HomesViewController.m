//
//  HomesViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomesViewController.h"
#import "HomeDetailViewController.h"
#import "HomeKitManager.h"

@interface HomesViewController () {
    NSMutableArray *homes;
}
@property (nonatomic, assign) HMHome *activeHome;
@end

@implementation HomesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[HomeKitManager sharedManager] setManagerDelegate:self];
    
    self.title = @"Homes";
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadData {
    homes = [[NSMutableArray alloc] initWithArray:[[HomeKitManager sharedManager] getHomes]];
    
    if([HomeKitManager sharedManager].currentHome == nil) {
        [HomeKitManager sharedManager].currentHome = [[HomeKitManager sharedManager] getPrimaryHome];
    }
    [self.tableView reloadData];
}


-(void) addNewHome {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add New Home" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){

        UITextField *textField = alertController.textFields[0];
        [[HomeKitManager sharedManager] addHome:textField.text completion:^(HMHome *home, NSError *error) {
            if (error) {
                NSLog(@"Error adding new home: %@", error);
            } else {
                NSLog(@"New home added.");
                [self addHome:home];
            }
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) addHome:(HMHome*)home {
    NSMutableArray *newHomes = [NSMutableArray arrayWithArray:homes];
    [newHomes addObject:home];
    
    homes = [NSMutableArray arrayWithArray:newHomes];
    [self.tableView reloadData];
}


#pragma mark - HMHomeManagerDelegate

- (void)homeManagerDidUpdateHomes:(HMHomeManager *)manager {
    [self loadData];
}

- (void)homeManager:(HMHomeManager *)manager didRemoveHome:(HMHome *)home {
    
    if([home isEqual:[HomeKitManager sharedManager].currentHome]) {
        [HomeKitManager sharedManager].currentHome = nil;
    }
    [self loadData];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1)
        return @"Primary Home";
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return homes.count + 1;
    if (section == 1)
        return MAX(homes.count,1);
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0 && indexPath.row == homes.count) {
        return [self.tableView dequeueReusableCellWithIdentifier:@"AddHomeCell"];
    }
    else if(homes == nil || [homes count] == 0) {
        return [self.tableView dequeueReusableCellWithIdentifier:@"NoHomesCell"];
    }
    
    
    NSString *reuseIdentifier;
    if (indexPath.section == 0) {
        reuseIdentifier = @"HomeCell";
    }
    if (indexPath.section == 1) {
        reuseIdentifier = @"PrimaryHomeCell";
    }
    
    
    HMHome *home = [homes objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = home.name;
    
    if (indexPath.section == 1) {
        if([home isEqual:[[HomeKitManager sharedManager] getPrimaryHome]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row != homes.count) {
        return (indexPath.section == 0);
    }
    return !([self cellTypeForIndexPath:indexPath] == Add);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        HMHome *homeToDelete = [homes objectAtIndex:indexPath.row];
        if([homeToDelete isEqual:[HomeKitManager sharedManager].currentHome]) {
            [HomeKitManager sharedManager].currentHome = [[HomeKitManager sharedManager] getPrimaryHome];
        }
        
        [homes removeObjectAtIndex:indexPath.row];
        [[HomeKitManager sharedManager] removeHome:homeToDelete completion:^(NSError *error){
            
            if(error) {
                NSLog(@"Error deleting home %@", homeToDelete);
                
            } else {
                
                NSLog(@"Delete Home");
                
                NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:1];
                [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }];
        [self.tableView reloadData];

    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0 && indexPath.row == homes.count) {
        [self addNewHome];
    }
    else if(indexPath.section == 1 && (homes == nil || [homes count] == 0)) {
        return;
    }
    else if(indexPath.section == 1) {
        [[HomeKitManager sharedManager] updatePrimaryHome:[homes objectAtIndex:indexPath.row] completion:^(NSError *error) {
            NSLog(@"Update Primary Home");

            NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:1];
            [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
    }
    else {
    
        [HomeKitManager sharedManager].currentHome = [homes objectAtIndex:indexPath.row];
//        [self performSegueWithIdentifier:@"Show Home" sender:self];
        [self performSegueWithIdentifier:@"Show Home Accessories" sender:self];
    }
}


#pragma mark - TableView datasource - Helper

-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath {
    
    if (indexPath.row == [homes count]) {
        return Add;
    }
    else {
        return Object;
    }
}

@end
