//
//  RoomsViewController
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitObjectsViewController.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface RoomsViewController : HomeKitObjectsViewController

@end
