//
//  ServicesViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 02.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TableViewNameCell.h"
#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface EditAccessoryViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, TableViewNameCellDelegate>

@property (nonatomic, strong) HMAccessory *accessory;
@property (nonatomic, assign) bool isAdding;


@end

