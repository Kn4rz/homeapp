//
//  HomeKitObjectsViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitObjectsViewController.h"
#import "HomeKitManager.h"

@implementation HomeKitObjectsViewController


#pragma mark - TableView datasource - Helper

-(NSString*)titleForAddRowInSection:(HomeKitObjectSection)objectType {
    switch (objectType) {
        case Accessory:
            return @"Add Accessory…";
            
        case Room:
            return @"Add Room…";
            
        case Zone:
            return @"Add Zone…";
            
        case ActionSet:
            return @"Add Scene…";
            
        case Trigger:
            return @"Add Trigger…";
            
        default:
            return nil;
    }
}

-(UITableViewCell*)addCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    NSString *reuseIdentifier = @"AddCell";
    
    UITableViewCell *cell =  [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.textLabel.text = [self titleForAddRowInSection:self.objectType];
    return cell;
}

-(HomeCellType)cellTypeForIndexPath:(NSIndexPath*)indexPath {
    
    NSInteger objectCount = [self objectsForSection:self.objectType];
    if (indexPath.row == objectCount) {
        return Add;
    }
    else {
        return Object;
    }
}

-(NSString*)reuseIdentifierForCellForHomeKitObject:(HomeKitObjectSection)objectType {
    switch (objectType) {
            
        case Accessory:
            return @"AccessoryCell";
            
        case Room:
            return @"RoomCell";
            
        case Zone:
            return @"ZoneCell";
            
        case ActionSet:
            return @"ActionSetCell";
            
        case Trigger:
            return @"TriggerCell";
    }
}

-(NSInteger) objectsForSection:(HomeKitObjectSection)section {
    switch (section) {
            
        case Accessory:
            return [HomeKitManager sharedManager].currentHome.accessories.count;
            
        case Room:
            return [HomeKitManager sharedManager].currentHome.rooms.count + 1; // +1 Room for entire Home
            
        case Zone:
            return [HomeKitManager sharedManager].currentHome.zones.count;
            
        case ActionSet:
            return [HomeKitManager sharedManager].currentHome.actionSets.count;
            
        case Trigger:
            return [HomeKitManager sharedManager].currentHome.triggers.count;
            
        default:
            return 0;
    }
}

@end
