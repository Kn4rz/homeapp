//
//  TableViewSwtichCell.h
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TableViewSwitchCellDelegate <NSObject>
-(void) switchDidChanged;
@end


@interface TableViewSwitchCell : UITableViewCell

+ (UITableView *)styleTableView:(UITableView *)_tableView forId:(NSString *)cellId;
@property (weak, nonatomic) IBOutlet UISwitch *enabledSwitch;

@property (nonatomic, weak) id<TableViewSwitchCellDelegate> _delegate;

@end
