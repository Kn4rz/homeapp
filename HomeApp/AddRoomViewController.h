//
//  AddRoomViewController.h
//  HomeApp
//
//  Created by Markus Drechsel on 03.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HomeKit/HomeKit.h>

@interface AddRoomViewController : UITableViewController <HMHomeManagerDelegate, HMHomeDelegate, HMAccessoryDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) HMZone *zone;

@end