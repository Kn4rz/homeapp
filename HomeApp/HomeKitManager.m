//
//  HomeKitManager.m
//  HomeKit_KnarzSample
//
//  Created by Markus Drechsel on 01.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "HomeKitManager.h"
#import <HomeKit/HomeKit.h>


@implementation HomeKitManager

+ (HomeKitManager *)sharedManager {
    static HomeKitManager *_sharedManager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

- (id)init {
    if (self = [super init]) {
        homeManager = [[HMHomeManager alloc] init];
    }
    return self;
}

-(HMHomeManager*)getManager {
    return homeManager;
}

-(void)setManagerDelegate:(id<HMHomeManagerDelegate>)delegate {
    homeManager.delegate = delegate;
}

-(void)setAccesoryBrowserDelegate:(id<HMAccessoryBrowserDelegate>)delegate {
    accessoryBrowser = [[HMAccessoryBrowser alloc] init];
    accessoryBrowser.delegate = delegate;
}


#pragma mark - Accessory Browser

-(NSArray*)getDiscoverdAccessories {
    return accessoryBrowser.discoveredAccessories;
}


#pragma mark - Home

-(NSArray*)getHomes {
    return homeManager.homes;
}

-(HMHome*)getPrimaryHome {
    return homeManager.primaryHome;
}

-(void)updatePrimaryHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [homeManager updatePrimaryHome:home completionHandler:handler];
    self.primaryHome = homeManager.primaryHome;
}

-(void)addHome:(NSString*)homeName completion:(void (^)(HMHome *home, NSError *error))handler {
    [homeManager addHomeWithName:homeName completionHandler:handler];
}

-(void)removeHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [homeManager removeHome:home completionHandler:handler];
}

-(void)renameHome:(NSString*)newHomeName toHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home updateName:newHomeName completionHandler:handler];
}

#pragma mark - Accessory

- (void)startSearchingForNewAccessories {
    [accessoryBrowser startSearchingForNewAccessories];
}

- (void)stopSearchingForNewAccessories {
    [accessoryBrowser stopSearchingForNewAccessories];
}

-(void)addAccessory:(HMAccessory*)accessory toHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home addAccessory:accessory completionHandler:handler];
}

-(void)removeAccessory:(HMAccessory*)accessory fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home removeAccessory:accessory completionHandler:handler];
}

-(void)renameAccessory:(NSString*)newAccessoryName inAccessory:(HMAccessory*)accessory completion:(void (^)(NSError *error))handler {
    [accessory updateName:newAccessoryName completionHandler:handler];
}

- (void)assignAccessory:(HMAccessory *)accessory toRoom:(HMRoom *)room inHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home assignAccessory:accessory toRoom:room completionHandler:handler];
}

#pragma mark - Room

-(HMRoom*)getRoomForEntireHome {
    return [self.currentHome roomForEntireHome];
}

- (void)addRoom:(NSString *)roomName toHome:(HMHome*)home completion:(void (^)(HMRoom *room, NSError *error))handler {
    [home addRoomWithName:roomName completionHandler:handler];
}

- (void)removeRoom:(HMRoom *)room fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home removeRoom:room completionHandler:handler];
}

- (void)renameRoom:(NSString *)newRoomName inRoom:(HMRoom*)room completion:(void (^)(NSError * error))handler {
    [room updateName:newRoomName completionHandler:handler];
}


#pragma mark - Zone

- (void)addZone:(NSString *)zoneName toHome:(HMHome*)home completion:(void (^)(HMZone *zone, NSError *error))handler {
    [home addZoneWithName:zoneName completionHandler:handler];
}

- (void)removeZone:(HMZone *)zone fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home removeZone:zone completionHandler:handler];
}

- (void)addRoomToZone:(HMRoom *)room toZone:(HMZone*)zone completion:(void (^)(NSError * error))handler {
    [zone addRoom:room completionHandler:handler];
}

- (void)removeRoomFromZone:(HMRoom *)room fromZone:(HMZone*)zone completion:(void (^)(NSError * error))handler {
    [zone removeRoom:room completionHandler:handler];
}

- (void)renameZone:(NSString *)newZoneName inZone:(HMZone*)zone completion:(void (^)(NSError * error))handler {
    [zone updateName:newZoneName completionHandler:handler];
}


#pragma mark - ActionSet (Scene)

- (void)addActionSet:(NSString *)actionSetName toHome:(HMHome*)home completion:(void (^)(HMActionSet *actionSet, NSError *error))handler {
    [home addActionSetWithName:actionSetName completionHandler:handler];
}

- (void)removeActionSet:(HMActionSet *)actionSet fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home removeActionSet:actionSet completionHandler:handler];
}

- (void)renameActionSet:(NSString *)newActionSetName inActionSet:(HMActionSet*)actionSet completion:(void (^)(NSError * error))handler {
    [actionSet updateName:newActionSetName completionHandler:handler];
}


#pragma mark - Trigger

- (void)addTrigger:(HMTrigger *)trigger toHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home addTrigger:trigger completionHandler:handler];
}
- (void)removeTrigger:(HMTrigger *)trigger fromHome:(HMHome*)home completion:(void (^)(NSError *error))handler {
    [home removeTrigger:trigger completionHandler:handler];
}

- (void)renameTrigger:(NSString *)newTriggerName inTrigger:(HMTrigger*)trigger completion:(void (^)(NSError * error))handler {
    [trigger updateName:newTriggerName completionHandler:handler];
}

@end