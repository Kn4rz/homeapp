//
//  ServicesViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 02.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "EditAccessoryViewController.h"
#import "TableViewNameCell.h"
#import "HomeKitManager.h"

@interface EditAccessoryViewController () {
    UIBarButtonItem *saveButton;
    UITextField *_nameField;
    
    NSMutableArray *rooms;
    
    HMRoom *selectedRoom;
}
@end


@implementation EditAccessoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = self.accessory.name;
    selectedRoom = self.accessory.room;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NameCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NameCell"];
    
    saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    saveButton.enabled = NO;
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] init];
    addButton.title = @"Add";
    [addButton setAction:@selector(addButtonPressed)];
    [addButton setTarget:self];
    
    self.navigationItem.rightBarButtonItem = self.isAdding ? addButton : saveButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadData {
    rooms = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.rooms];
    [rooms addObject:[[HomeKitManager sharedManager] getRoomForEntireHome]];
    [self.tableView reloadData];
}


-(void)enableSaveButtonIfNecessary {
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    saveButton.enabled = (trimmedName.length > 0 && ![self.accessory.name isEqualToString:trimmedName])
                        || ![self.accessory.room isEqual:selectedRoom];
}

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveButtonPressed {
    
    NSString *trimmedName =[_nameField.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    [[HomeKitManager sharedManager] renameAccessory:trimmedName inAccessory:self.accessory completion:^(NSError *error) {
        if (error) {
            NSLog(@"Error renaming accessory: %@", error);
        } else {
            NSLog(@"Accessory renamed");
            if ([_nameField isFirstResponder]) {
                [_nameField resignFirstResponder];
            }
        }
    }];
    
    [[HomeKitManager sharedManager] assignAccessory:self.accessory toRoom:selectedRoom inHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error) {
        if (error) {
            NSLog(@"Error assigning asscessory: %@", error);
        } else {
            NSLog(@"Accessory assigned to new Room.");
        }
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)addButtonPressed {
    
    [[HomeKitManager sharedManager] addAccessory:self.accessory toHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        
        if (error) {
            NSLog(@"error in adding accessory: %@", error);
        } else {
            
            if(selectedRoom == nil) {
                selectedRoom = [[HomeKitManager sharedManager] getRoomForEntireHome];
            }
            
            [[HomeKitManager sharedManager] renameAccessory:_nameField.text inAccessory:self.accessory completion:^(NSError *error) {
                if (error) {
                    NSLog(@"Error renaming accessory: %@", error);
                } else {
                    NSLog(@"Accessory renamed");
                }
            }];
            
            [[HomeKitManager sharedManager] assignAccessory:self.accessory toRoom:selectedRoom inHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error) {
                if (error) {
                    NSLog(@"Error assigning asscessory: %@", error);
                } else {
                    NSLog(@"Accessory assigned to new Room.");
                }
            }];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}


#pragma mark - TableViewNameCell Delegate

-(void)nameFieldDidChanged {
    [self enableSaveButtonIfNecessary];
}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return nil;
    }
    
    if (rooms == nil || rooms.count == 0) {
        return nil;
    }
    return @"Assigned Room";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    }
    return rooms.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    NSLog(@"accessories: %@", accessories);
    
    if(indexPath.section == 0) {
        TableViewNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
        if (cell == nil) {
            cell = [[TableViewNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
        }
        _nameField = cell.nameField;
        cell._delegate = self;
        
        cell.nameField.text = self.accessory.name;
        return cell;
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"RoomCell"];
    HMRoom *room = [rooms objectAtIndex:indexPath.row];
    cell.textLabel.text = [room isEqual:[[HomeKitManager sharedManager] getRoomForEntireHome]] ? [NSString stringWithFormat:@"%@ (Default Room)",room.name] : room.name;
    
    cell.accessoryType = [room isEqual:selectedRoom] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRoom = [rooms objectAtIndex:indexPath.row];
    [self enableSaveButtonIfNecessary];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


@end