//
//  TimeConditionViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 15.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "TimeConditionViewController.h"
#import "SegmentedTimeCell.h"
#import "TableViewDatePickerCell.h"
#import <HomeKit/HomeKit.h>

@interface TimeConditionViewController () {
    
    UIDatePicker *_datePicker;
    
    TimeConditionType timeType;
    TimeConditionOrder order;
    TimeConditionSunState sunState;
    
    NSMutableArray *beforeOrAfterTitles;
    NSMutableArray *sunriseSunsetTitles;
}

@end


@implementation TimeConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DatePickerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DatePickerCell"];
    
    timeType = Time;
    order = Before;
    sunState = Sunrise;
    
    beforeOrAfterTitles = [[NSMutableArray alloc] initWithObjects:@"Before", @"After", @"At", nil];
    sunriseSunsetTitles = [[NSMutableArray alloc] initWithObjects:@"Sunrise", @"Sunset", nil];
    
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] init];
    saveButton.title = @"Save";
    [saveButton setAction:@selector(saveButtonPressed)];
    [saveButton setTarget:self];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] init];
    cancelButton.title = @"Cancel";
    [cancelButton setAction:@selector(cancelButtonPressed)];
    [cancelButton setTarget:self];
    self.navigationItem.leftBarButtonItem = cancelButton;
}


#pragma mark - Button Actions

-(void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveButtonPressed {
    
    NSPredicate *predicate;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:_datePicker.date];
    NSString *significantEventString = (sunState == Sunrise) ? HMSignificantEventSunrise : HMSignificantEventSunset;
    
    switch (timeType) {
        case Time:
            
            switch (order) {
                case Before:
                    predicate = [HMEventTrigger predicateForEvaluatingTriggerOccurringBeforeDateWithComponents:dateComponents];
                    break;
                    
                case After:
                    predicate = [HMEventTrigger predicateForEvaluatingTriggerOccurringAfterDateWithComponents:dateComponents];
                    break;
                    
                case At:
                    predicate = [HMEventTrigger predicateForEvaluatingTriggerOccurringOnDateWithComponents:dateComponents];
                    break;
            }
            break;
            
        case Sun:
            switch (order) {
                case Before:
                    predicate = [HMEventTrigger predicateForEvaluatingTriggerOccurringBeforeSignificantEvent:significantEventString applyingOffset:nil];
                    break;
                    
                case After:
                    predicate = [HMEventTrigger predicateForEvaluatingTriggerOccurringAfterSignificantEvent:significantEventString applyingOffset:nil];
                    break;
                    
                case At:
                    break;
            }
            break;
            
        default:
            break;
    }
    
    if(predicate != nil) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewConditionCreated" object:predicate userInfo:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - SegmentControl change methods

-(void)segmentedControlDidChange:(UISegmentedControl*)segmentedControl {
    timeType = [self timeConditionTypeAtIndex:segmentedControl.selectedSegmentIndex];
    [self reloadDynamicSections];
}

-(void)reloadDynamicSections {
    if(timeType == Sun && order == At) {
        order = Before;
    }
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(1, 2)];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(TimeConditionType)timeConditionTypeAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return Time;
            
        case 1:
            return Sun;
            
        default:
            return Time;
    }
}


#pragma mark - TableViewDatePickerCell Delegate

-(void) datePickerDidChanged {

}


#pragma mark - TableView datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
        case TimeOrSun:
            return @"Condition Type";
            
        case BeforeOrAfter:
            return nil;
            
        case Value:
            
            if(timeType == Time) {
                return @"Time";
            
            } else {
                return @"Event";
            }
            
        default:
            return @"undefined";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self sectionForIndex:indexPath.section] == Value) {
        switch (timeType) {
            case Time:
                return 163.0f;
                
            default:
                return UITableViewAutomaticDimension;
        }
    }
    
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch ([self sectionForIndex:indexPath.section]) {
        case TimeOrSun:
            return [self segmentedCellForRowAtIndexPath:indexPath];

        case BeforeOrAfter:
            return [self selectionCellForRowAtIndexPath:indexPath];
            
        case Value:
            
            switch (timeType) {
                case Time:
                    return [self datePickerCellForRowAtIndexPath:indexPath];
                    
                case Sun:
                    return [self selectionCellForRowAtIndexPath:indexPath];
            }
            
        default:
            return nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([self sectionForIndex:section]) {
            
        case TimeOrSun:
            return 1;
            
        case BeforeOrAfter:
            return timeType == Time ? 3 : 2;
            
        case Value:
            return timeType == Time ? 1 : 2;
            
        default:
            return 1;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case BeforeOrAfter:
            [self didSelectBeforeOrAfterSelectionAtIndexPath:indexPath];
            break;
            
        case Value:
            if(timeType == Sun) {
                [self didSelectSunStateSelectionAtIndexPath:indexPath];
            }
            break;
            
        default:
            break;
    }
}


#pragma mark - TableView datasource - Helper

-(TimeConditionTableViewSection)sectionForIndex:(NSInteger)section {
    switch (section) {
        case 0:
            return TimeOrSun;
            
        case 1:
            return BeforeOrAfter;
            
        case 2:
            return Value;
            
        default:
            return UnknownSection;
    }
}

-(UITableViewCell*)segmentedCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    SegmentedTimeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SegmentedTimeCell"];
    if (cell == nil) {
        cell = [[SegmentedTimeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SegmentedTimeCell"];
    }
    
    cell.segmentedControl.selectedSegmentIndex = Time;
    [cell.segmentedControl removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [cell.segmentedControl addTarget:self action:@selector(segmentedControlDidChange:) forControlEvents:UIControlEventValueChanged];
    
    return cell;
}

-(UITableViewCell*)selectionCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
    
    switch ([self sectionForIndex:indexPath.section]) {
        case BeforeOrAfter:
            cell.textLabel.text = [beforeOrAfterTitles objectAtIndex:indexPath.row];
            cell.accessoryType = order == indexPath.row ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            break;
            
        case Value:
            if(timeType == Sun) {
                cell.textLabel.text = [sunriseSunsetTitles objectAtIndex:indexPath.row];
                cell.accessoryType = sunState == indexPath.row ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            }
            
        default:
            break;
    }
    
    return cell;
}

-(TableViewDatePickerCell*)datePickerCellForRowAtIndexPath:(NSIndexPath*)indexPath {
    TableViewDatePickerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DatePickerCell"];
    if (cell == nil) {
        cell = [[TableViewDatePickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DatePickerCell"];
    }
    _datePicker = cell.datePicker;
    cell._delegate = self;
    cell.datePicker.datePickerMode = UIDatePickerModeTime;
    
    return cell;
}

-(void)didSelectBeforeOrAfterSelectionAtIndexPath:(NSIndexPath*)indexPath {
    
    switch (indexPath.row) {
        case 0:
            order = Before;
            break;
            
        case 1:
            order = After;
            break;
            
        case 2:
            order = At;
            break;
    }
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)didSelectSunStateSelectionAtIndexPath:(NSIndexPath*)indexPath {
    
    switch (indexPath.row) {
        case 0:
            sunState = Sunrise;
            break;
            
        case 1:
            sunState = Sunset;
            break;
    }
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


@end
