//
//  CharacteristicStringFormatter.m
//  HomeApp
//
//  Created by Markus Drechsel on 12.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "CharacteristicStringFormatter.h"
#import <HomeKit/HomeKit.h>

@implementation CharacteristicStringFormatter

+(NSString*)localizedStringFromCharacteristic:(HMCharacteristic*)characteristic withValue:(id)value {
    
    CharacteristicStringFormatter *formatter = [[CharacteristicStringFormatter alloc] init];
    
    return [NSString stringWithFormat:@"%@ → %@", characteristic.localizedDescription, [formatter localizedDescriptionCharacteristicValue:characteristic withValue:value]];
}

+(NSString*)localizedStringForCharacteristicValue:(id)value withCharacteristic:(HMCharacteristic*)characteristic {
    CharacteristicStringFormatter *formatter = [[CharacteristicStringFormatter alloc] init];
    
    return [formatter localizedDescriptionCharacteristicValue:characteristic withValue:value];
}

+(NSString*)localizedStringForCharacteristicType:(HMCharacteristic*)characteristic {
    CharacteristicStringFormatter *formatter = [[CharacteristicStringFormatter alloc] init];
    
    return [formatter localizedCharacteristicType:characteristic];
}

+(NSString*)predeterminedValueDescriptionForNumber:(HMCharacteristic*)characteristic {
    CharacteristicStringFormatter *formatter = [[CharacteristicStringFormatter alloc] init];
    
    return [formatter predeterminedValueDescriptionForNumber:[characteristic.value intValue] characteristic:characteristic];
}


-(NSString*)localizedDescriptionCharacteristicValue:(HMCharacteristic*)characteristic withValue:(id)value {
    
    // isWriteOnly
    if(![characteristic.properties containsObject:HMCharacteristicPropertyReadable] && [characteristic.properties containsObject:HMCharacteristicPropertyWritable]) {
        return @"Write-Only Characteristic";
        
    } else if([characteristic.metadata.format isEqualToString:HMCharacteristicMetadataFormatBool]) {
        return [value boolValue] ? @"On" : @"Off";
        
    }
    
    if(value != nil) {
        
        NSString *predeterminedValue = [self predeterminedValueDescriptionForNumber:[value intValue] characteristic:characteristic];
        
        if(predeterminedValue != nil) {
            return predeterminedValue;
            
        } else {
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setMaximumFractionDigits:1];
//        [formatter setRoundingMode: NSNumberFormatterRoundDown];
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            formatter.minimumFractionDigits = (int)(log10(1.0 / characteristic.metadata.stepValue.doubleValue));
            
            NSString *string = [formatter stringFromNumber:value];
            if(string != nil) {
                return [NSString stringWithFormat:@"%@%@", string, [self localizedUnitDecoration:characteristic.metadata]];
            }
        }
        
    }

    return [NSString stringWithFormat:@"%@", value];
}


-(NSString*)localizedUnitDecoration:(HMCharacteristicMetadata*)metadata {
    
    if ([metadata.units isEqualToString:HMCharacteristicMetadataUnitsCelsius]) {
        return @"℃";
    }
    
    if ([metadata.units isEqualToString:HMCharacteristicMetadataUnitsArcDegree]) {
        return @"º";
    }
    
    if ([metadata.units isEqualToString:HMCharacteristicMetadataUnitsFahrenheit]) {
        return @"℉";
    }
    
    if ([metadata.units isEqualToString:HMCharacteristicMetadataUnitsPercentage]) {
        return @"%";
    }
    
    return @"";
    
}

-(NSString*)localizedCharacteristicType:(HMCharacteristic*)characteristic {
    
    NSString *type = characteristic.localizedDescription;
    
    NSString *localizedDescription = nil;
    if(![characteristic.properties containsObject:HMCharacteristicPropertyWritable] &&
       [characteristic.properties containsObject:HMCharacteristicPropertyReadable]) {
        
        localizedDescription = @"Read Only";
        
    } else if(![characteristic.properties containsObject:HMCharacteristicPropertyReadable] &&
              [characteristic.properties containsObject:HMCharacteristicPropertyWritable]) {
        
        localizedDescription = @"Write Only";
    }
    
    if(localizedDescription != nil) {
        type = [NSString stringWithFormat:@"%@ (%@)", type, localizedDescription];
    }
    return type;
}


-(NSString*)predeterminedValueDescriptionForNumber:(int)value characteristic:(HMCharacteristic*)characteristic {
    
    NSString *characteristicType = characteristic.characteristicType;
    
    if([characteristicType isEqualToString:HMCharacteristicTypePowerState]
       || [characteristicType isEqualToString:HMCharacteristicTypeInputEvent]
       || [characteristicType isEqualToString:HMCharacteristicTypeOutputState]) {
        
        return (bool)value ? @"On" : @"Off";
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeOutletInUse]
       || [characteristicType isEqualToString:HMCharacteristicTypeMotionDetected]
       || [characteristicType isEqualToString:HMCharacteristicTypeAdminOnlyAccess]
       || [characteristicType isEqualToString:HMCharacteristicTypeAudioFeedback]
       || [characteristicType isEqualToString:HMCharacteristicTypeObstructionDetected]) {
        
        return (bool)value ? @"Yes" : @"No";
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeTargetDoorState]
       || [characteristicType isEqualToString:HMCharacteristicTypeCurrentDoorState]) {
        
        switch (value) {
            case HMCharacteristicValueDoorStateOpen:
                return @"Open";
                
            case HMCharacteristicValueDoorStateOpening:
                return @"Opening";
                
            case HMCharacteristicValueDoorStateClosed:
                return @"Closed";
                
            case HMCharacteristicValueDoorStateClosing:
                return @"Closing";
                
            case HMCharacteristicValueDoorStateStopped:
                return @"Stopped";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeTargetHeatingCooling]) {
        
        switch (value) {
            case HMCharacteristicValueHeatingCoolingOff:
                return @"Off";
                
            case HMCharacteristicValueHeatingCoolingHeat:
                return @"Heat";
                
            case HMCharacteristicValueHeatingCoolingCool:
                return @"Cool";
                
            case HMCharacteristicValueHeatingCoolingAuto:
                return @"Auto";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeCurrentHeatingCooling]) {
        
        switch (value) {
            case HMCharacteristicValueHeatingCoolingOff:
                return @"Off";
                
            case HMCharacteristicValueHeatingCoolingHeat:
                return @"Heating";
                
            case HMCharacteristicValueHeatingCoolingCool:
                return @"Cooling";
                
            case HMCharacteristicValueHeatingCoolingAuto:
                return @"Auto";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeTargetLockMechanismState]
       || [characteristicType isEqualToString:HMCharacteristicTypeCurrentLockMechanismState]) {
        
        switch (value) {
            case HMCharacteristicValueLockMechanismStateUnsecured:
                return @"Unsecured";
                
            case HMCharacteristicValueLockMechanismStateSecured:
                return @"Secured";
                
            case HMCharacteristicValueLockMechanismStateUnknown:
                return @"Unknown";
                
            case HMCharacteristicValueLockMechanismStateJammed:
                return @"Jammed";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeTemperatureUnits]) {
        
        switch (value) {
            case HMCharacteristicValueTemperatureUnitCelsius:
                return @"Celsius";
                
            case HMCharacteristicValueTemperatureUnitFahrenheit:
                return @"Fahrenheit";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeLockMechanismLastKnownAction]) {
        
        switch (value) {
            case HMCharacteristicValueLockMechanismLastKnownActionSecuredUsingPhysicalMovementInterior:
                return @"Interior Secured";
                
            case HMCharacteristicValueLockMechanismLastKnownActionUnsecuredUsingPhysicalMovementInterior:
                return @"Interior Secured";
                
            case HMCharacteristicValueLockMechanismLastKnownActionSecuredUsingPhysicalMovementExterior:
                return @"Exterior Secured";
                
            case HMCharacteristicValueLockMechanismLastKnownActionUnsecuredUsingPhysicalMovementExterior:
                return @"Exterior Unsecured";
                
            case HMCharacteristicValueLockMechanismLastKnownActionSecuredWithKeypad:
                return @"Keypad Secured";
                
            case HMCharacteristicValueLockMechanismLastKnownActionUnsecuredWithKeypad:
                return @"Keypad Unsecured";
                
            case HMCharacteristicValueLockMechanismLastKnownActionSecuredRemotely:
                return @"Secured Remotely";
                
            case HMCharacteristicValueLockMechanismLastKnownActionUnsecuredRemotely:
                return @"Unsecured Remotely";
                
            case HMCharacteristicValueLockMechanismLastKnownActionSecuredWithAutomaticSecureTimeout:
                return @"Secured Automatically";
                
            case HMCharacteristicValueLockMechanismLastKnownActionSecuredUsingPhysicalMovement:
                return @"Secured Using Physical Movement";
                
            case HMCharacteristicValueLockMechanismLastKnownActionUnsecuredUsingPhysicalMovement:
                return @"Unsecured Using Physical Movement";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeRotationDirection]) {
        
        switch (value) {
            case HMCharacteristicValueRotationDirectionClockwise:
                return @"Clockwise";
                
            case HMCharacteristicValueRotationDirectionCounterClockwise:
                return @"Counter Clockwise";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeAirParticulateSize]) {
        
        switch (value) {
            case HMCharacteristicValueAirParticulateSize10:
                return @"Size 10";
                
            case HMCharacteristicValueAirParticulateSize2_5:
                return @"Size 2.5";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypePositionState]) {
        
        switch (value) {
            case HMCharacteristicValuePositionStateOpening:
                return @"Opening";
                
            case HMCharacteristicValuePositionStateClosing:
                return @"Closing";
                
            case HMCharacteristicValuePositionStateStopped:
                return @"Stopped";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeCurrentSecuritySystemState]) {
        
        switch (value) {
            case HMCharacteristicValueCurrentSecuritySystemStateAwayArm:
                return @"Away";
                
            case HMCharacteristicValueCurrentSecuritySystemStateStayArm:
                return @"Home";
                
            case HMCharacteristicValueCurrentSecuritySystemStateNightArm:
                return @"Night";
                
            case HMCharacteristicValueCurrentSecuritySystemStateDisarmed:
                return @"Disarm";
                
            case HMCharacteristicValueCurrentSecuritySystemStateTriggered:
                return @"Triggered";
        }
    }
    
    if([characteristicType isEqualToString:HMCharacteristicTypeTargetSecuritySystemState]) {
        
        switch (value) {
            case HMCharacteristicValueTargetSecuritySystemStateAwayArm:
                return @"Away";
                
            case HMCharacteristicValueTargetSecuritySystemStateStayArm:
                return @"Home";
                
            case HMCharacteristicValueTargetSecuritySystemStateNightArm:
                return @"Night";
                
            case HMCharacteristicValueTargetSecuritySystemStateDisarm:
                return @"Disarm";
                
        }
    }
    
    return nil;
}

@end
