//
//  RoomsViewController.m
//  HomeApp
//
//  Created by Markus Drechsel on 04.02.16.
//  Copyright © 2016 Markus Drechsel. All rights reserved.
//

#import "RoomsViewController.h"
#import "EditRoomViewController.h"
#import "HomeKitManager.h"

@interface RoomsViewController () {
    NSMutableArray *rooms;
}
@end


@implementation RoomsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Rooms";
    self.objectType = Room;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Loading Data

-(void) loadData {
    rooms = [[NSMutableArray alloc] initWithArray:[HomeKitManager sharedManager].currentHome.rooms];
    if([[HomeKitManager sharedManager] getRoomForEntireHome] != nil) {
        [rooms addObject:[[HomeKitManager sharedManager] getRoomForEntireHome]];
    }
    [self.tableView reloadData];
}


#pragma mark - Room creation methods

-(void)showNewRoomView {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add New Room" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UITextField *textField = alertController.textFields[0];
        
        [[HomeKitManager sharedManager] addRoom:textField.text toHome:[HomeKitManager sharedManager].currentHome completion:^(HMRoom *room, NSError *error) {
            if (error) {
                NSLog(@"Error adding new room: %@", error);
            } else {
                NSLog(@"New room added.");
                [self addRoom:room];
            }
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)addRoom:(HMRoom*)room {
    NSMutableArray *newRooms = [NSMutableArray arrayWithArray:rooms];
    [newRooms addObject:room];
    rooms = [NSMutableArray arrayWithArray:newRooms];
    
    NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
    [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Room delete methods

-(void)deleteRoom:(HMRoom*)room {
    [[HomeKitManager sharedManager] removeRoom:room fromHome:[HomeKitManager sharedManager].currentHome completion:^(NSError *error){
        NSLog(@"Delete Room from Home");
        
        [rooms removeObject:room];
        NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:0];
        [self.tableView reloadSections:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}


#pragma mark - TableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return rooms.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([self cellTypeForIndexPath:indexPath]) {
        case Add:
            return [self addCellForRowAtIndexPath:indexPath];
            
        case Object:
            return [self homeKitObjectCellForRowAtIndexPath:indexPath];
            
        default:
            return nil;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row != rooms.count) {
        return !([[[HomeKitManager sharedManager] getRoomForEntireHome] isEqual:[rooms objectAtIndex:indexPath.row]]
                 || [self cellTypeForIndexPath:indexPath] == Add);
    }
    return !([self cellTypeForIndexPath:indexPath] == Add);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self deleteRoom:[rooms objectAtIndex:indexPath.row]];
        
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self cellTypeForIndexPath:indexPath] == Add){
        [self showNewRoomView];
    }
}


#pragma mark - TableView datasource - Helper

-(UITableViewCell*)homeKitObjectCellForRowAtIndexPath:(NSIndexPath*)indexPath {
 
    HMRoom *room = [rooms objectAtIndex:indexPath.row];
    NSString *name = [room isEqual:[[HomeKitManager sharedManager] getRoomForEntireHome]] ? [NSString stringWithFormat:@"%@ (Default Room)",room.name] : room.name;
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[self reuseIdentifierForCellForHomeKitObject:Room]];
    cell.textLabel.text = name;
    return cell;
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UITableViewCell class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        if ([segue.identifier isEqualToString:@"Show Room"]) {
            EditRoomViewController *rvc = (EditRoomViewController*)segue.destinationViewController;
            rvc.room = [rooms objectAtIndex:indexPath.row];
        }
        
    }
}

@end
